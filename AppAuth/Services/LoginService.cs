﻿using AppAuth.Models;
using AppAuth.Repositories;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;

namespace AppAuth.Services
{
    public class LoginService
    {
        private static string loginUrl = "https://appcloud.fpcetg.com.tw/mobilefpc/api/common/login";
        private static string tokenAuthUrl = "https://appcloud.fpcetg.com.tw/mobilefpc/api/common/tokeninformationForCRM";

        public static LoginResponse Login(LoginRequest request)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string jsonText = serializer.Serialize(request);
            using (WebClient wc = new WebClient())
            {
                try
                {
                    wc.Encoding = Encoding.UTF8;
                    wc.Headers.Add(HttpRequestHeader.ContentType, "application/json");
                    var response = wc.UploadData(loginUrl, "POST", Encoding.UTF8.GetBytes(jsonText));
                    var login = (LoginResponse)serializer.Deserialize(Encoding.UTF8.GetString(response), typeof(LoginResponse));

                    if (login.result == "SUCCESS")
                    {
                        login.admin = SystemLogin(login.account);
                        if (login.admin.Count().Equals(0))
                        {
                            login.result = "沒有使用權限!";
                        }
                    }

                    return login;
                }
                catch (WebException ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }

        public static TokenModels MobileAuth(Guid token)
        {
            using (WebClient wc = new WebClient())
            {
                try
                {
                    wc.Encoding = Encoding.UTF8;
                    wc.Headers.Add(HttpRequestHeader.ContentType, "application/x-www-form-urlencoded");
                    NameValueCollection nc = new NameValueCollection();
                    nc["accessToken"] = token.ToString();

                    var response = wc.UploadValues(tokenAuthUrl, nc);
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    return (TokenModels)serializer.Deserialize(Encoding.UTF8.GetString(response), typeof(TokenModels));
                }
                catch (WebException ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }

        public static List<admin> SystemLogin(string account)
        {
            return AdminRepository.getUserPermission(account);
        }
    }

    public class LoginRequest
    {
        public string account { get; set; }
        public string password { get; set; }
        public int platformNumber { get; set; } = 3;
        public string appVersion { get; set; } = "9.9.9";
        public string model { get; set; } = "Web";
        public string OSVersion { get; set; } = "1.0.0";
        public string deviceID { get; set; } = "123467890";
        public int isDebugMode { get; set; } = 0;
    }

    public class LoginResponse
    {
        public string result { get; set; }
        public int aid { get; set; }
        public string account { get; set; }
        public string name { get; set; }
        public string accessToken { get; set; }
        public string refreshToken { get; set; }

        public List<admin> admin { get; set; }
    }

    public class TokenModels
    {
        public string result { get; set; }

        public UserModels data { get; set; }
    }

    public class UserModels
    {
        public string account { get; set; }
        public string name { get; set; }
        public string companyID { get; set; }
        public string departID { get; set; }
        public string departName { get; set; }
        public string position { get; set; }
        public string ExtRealOrgId1 { get; set; }
        public string OrgSupport { get; set; }
    }
}

