﻿using Dapper;
using MySql.Data.MySqlClient;
using System.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace AppAuth.Services
{
    public class DbService
    {
        private static Dictionary<string, string> dbInfo = new Dictionary<string, string>()
        {
            { "joe", "10.153.196.100" },
            { "app_permission", "10.153.196.157" },
            { "JWETG", "10.110.196.185"}
        };

        private static Dictionary<string, string> dbAccount = new Dictionary<string, string>()
        {
            { "joe", "joe" },
            { "app_permission", "apppem" },
            { "JWETG", "mlfpcread"}
        };

        private static Dictionary<string, string> dbPassword = new Dictionary<string, string>()
        {
            { "joe", "`1qaz2wsx" },
            { "app_permission", "dxsx547289" },
            { "JWETG", "mlfpcread"}
        };

        private static string strConn = String.Empty;

        public static List<T> Query<T>(bool isMssql, string sql, object obj = null)
        {
            List<T> result = new List<T>();
            DbInit(isMssql);

            if (isMssql)
            {
                using (SqlConnection conn = new SqlConnection(strConn))
                {
                    result = conn.Query<T>(sql, obj).ToList();
                }
            }
            else
            {
                using (MySqlConnection conn = new MySqlConnection(strConn))
                {
                    result = conn.Query<T>(sql, obj).ToList();
                }
            }

            return result;
        }

        public static void ExecuteNonQuery(string sql, object obj = null)
        {
            DbInit();
            using (MySqlConnection conn = new MySqlConnection(strConn))
            {
                if (true) {
                    conn.Execute(sql, obj);
                }
            }
        }

        public static T ExecuteScalar<T>(string sql, object obj = null)
        {
            DbInit();
            using (MySqlConnection conn = new MySqlConnection(strConn))
            {
                //return (T)conn.ExecuteScalar(sql, obj);
                return default(T);
            }
        }

        private static void DbInit(bool isMssql = false)
        {
            string dbName = isMssql ? "JWETG" : (isTest() ? "joe" : "app_permission");

            strConn = String.Format("data source ={0}; initial catalog = {1}; user id = {2}; password = {3}; Connection Timeout=180"
                             , dbInfo[dbName]
                             , dbName
                             , dbAccount[dbName]
                             , dbPassword[dbName]);
        }

        public static bool isTest()
        {
            return Convert.ToBoolean(WebConfigurationManager.AppSettings["isTest"]);
        }
    }
}