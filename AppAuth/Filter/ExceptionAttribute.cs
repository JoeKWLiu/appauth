﻿using AppAuth.Models;
using AppAuth.Repositories;
using AppAuth.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http.Filters;

namespace AppAuth.Filter
{
    public class ExceptionAttribute : ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext actionExecutedContext)
        {
            string msg = $"Exception captured by WebApiExceptionFilter: {actionExecutedContext.Exception.GetBaseException().Message}";

            var response = actionExecutedContext.Request.CreateErrorResponse(HttpStatusCode.BadRequest, msg);
            actionExecutedContext.Response = response;

            string controller = actionExecutedContext.ActionContext.ControllerContext.ControllerDescriptor.ControllerName;
            string action = actionExecutedContext.ActionContext.ActionDescriptor.ActionName;
            string data = String.Empty;

            var keys = actionExecutedContext.ActionContext.ActionArguments;

            if (keys.Count > 0)
            {
                dynamic am = actionExecutedContext.ActionContext.ActionArguments.FirstOrDefault().Value;

                log lg = new log();

                if (am is LoginRequest)
                {
                    lg.CreatedBy = am.account;
                }
                else
                {
                    lg.CreatedBy = am.auth.token.data.account;
                }

                lg.Controller = controller;
                lg.Action = action;
                lg.Status = actionExecutedContext.Response.StatusCode.ToString();
                lg.Msg = msg;
                lg.Param = JsonConvert.SerializeObject(keys);
                lg.CreatedOn = DateTime.Now;

                LogRepository.Log(lg);
            }
        }
    }
}