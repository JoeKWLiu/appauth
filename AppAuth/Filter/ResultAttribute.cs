﻿using AppAuth.Models;
using AppAuth.Repositories;
using AppAuth.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Filters;

namespace AppAuth.Filter
{
    public class ResultAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
        {
            if (actionExecutedContext.Exception != null)
            {
                return;
            }

            string controller = actionExecutedContext.ActionContext.ControllerContext.ControllerDescriptor.ControllerName;
            string action = actionExecutedContext.ActionContext.ActionDescriptor.ActionName;
            string data = String.Empty;

            var keys = actionExecutedContext.ActionContext.ActionArguments;

            if (keys.Count > 0)
            {
                dynamic am = actionExecutedContext.ActionContext.ActionArguments.FirstOrDefault().Value;

                log lg = new log();

                if (am is LoginRequest)
                {
                    lg.CreatedBy = am.account;
                }
                else
                {
                    lg.CreatedBy = am.auth.token.data.account;
                }

                lg.Controller = controller;
                lg.Action = action;
                lg.Param = JsonConvert.SerializeObject(am);
                lg.Status = actionExecutedContext.Response.StatusCode.ToString();
                lg.Msg = String.Empty;
                lg.CreatedOn = DateTime.Now;
                LogRepository.Log(lg);
            }
        }
    }
}