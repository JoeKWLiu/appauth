﻿using AppAuth.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace AppAuth.Filter
{
    public class AuthorizeAttribute : AuthorizationFilterAttribute
    {
        public override void OnAuthorization(HttpActionContext actionContext)
        {
            if (actionContext.ActionArguments.Count > 0 && actionContext.ActionArguments.FirstOrDefault().Key.Equals("request"))
            {
                RequestModel request = (RequestModel)actionContext.ActionArguments["request"];

                if (request.auth.token is null || request.auth.token.data is null)
                {
                    throw new HttpResponseException(System.Net.HttpStatusCode.Forbidden);
                }
            }

            base.OnAuthorization(actionContext);
        }
    }
}