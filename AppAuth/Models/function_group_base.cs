﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AppAuth.Models
{
    public class function_group_base
    {
        [Key]
        public int appID { get; set; }
        [Key]
        public int functionGroupID { get; set; }
        public string functionGroupCode { get; set; }
        public string name { get; set; } = "";
        public int sortOrder { get; set; } = 0;
        public string uuid { get; set; } = "";

        public bool isEnabled { get; set; }
        public int ucount { get; set; }
        public int dcount { get; set; }
        public int gcount { get; set; }
        public int scount { get; set; }

        public bool editable { get; set; }
    }
}