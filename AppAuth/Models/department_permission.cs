﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AppAuth.Models
{
    public class department_permission
    {
        public string companyID { get; set; }
        public string departmentID { get; set; }
        public string regionID { get; set; } = "-1";
        public int appID { get; set; }
        public int functionGroupID { get; set; }
        public int platformID { get; set; }
        public int functionID { get; set; }
        public int level { get; set; }
        public bool isEnabled { get; set; }
        public string comment { get; set; }
        [Key]
        public string uuid { get; set; }

        //-----------------------------------
        public string name { get; set; }
        public string fullUnitID { get; set; }
    }
}