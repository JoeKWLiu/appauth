﻿using AppAuth.Repositories;
using AppAuth.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppAuth.Models
{
    public class AuthModel
    {
        private Guid _accessToken;

        public Guid accessToken {
            set
            {
                _accessToken = value;
                token = LoginService.MobileAuth(_accessToken);
                privilege = AdminRepository.getUserPermission(token.data.account);
            }
            get
            {
                return _accessToken;
            }
        }
        public TokenModels token { get; private set; }
        public IEnumerable<admin> privilege { get; private set; }
    }
}