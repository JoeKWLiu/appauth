﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AppAuth.Models.mlfpcread
{
    public class Func_OM_HGM_ViewGroup
    {
        public int Pid { get; set; }
        [Key]
        public string Guid { get; set; }
        public string GroupName { get; set; }
        public string Owner { get; set; }
        public string OwnerGuid { get; set; }
        public bool IsSys { get; set; }
        public string Remark { get; set; }
        public string AddIdno { get; set; }
        public DateTime AddTime { get; set; }
        public string DelIdno { get; set; }
        public DateTime? DelTime { get; set; }
        public int Num { get; set; }
        public DateTime LastUpdateTime { get; set; }

    }
}