﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppAuth.Models
{
    public class department
    {
        public string companyID { get; set;}
        public string departID { get; set; }
        public string departName { get; set; }
    }
}