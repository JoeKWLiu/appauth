﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppAuth.Models
{
    public class log
    {
        public string CreatedBy { get; set; }
        public string Controller { get; set; }
        public string Action { get; set; }
        public string Param { get; set; }
        public string Status { get; set; }
        public string Msg { get; set; }
        public DateTime CreatedOn { get; set; }
    }
}