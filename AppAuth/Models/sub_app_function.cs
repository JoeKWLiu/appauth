﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppAuth.Models
{
    public class sub_app_function
    {
        public string mainUUID { get; set; }
        public int functionID { get; set; }
        public string uuid { get; set; }
        public string functionCode { get; set; }
        public string name { get; set; }
        public string comment { get; set; }
        public bool defaultPermission { get; set; }
        public bool isEnabled { get; set; }
        public int sortOrder { get; set; }
        public string iconURL { get; set; }
        public string webURL { get; set; }
        

        public int appID { get; set; }
        public int functionGroupID { get; set; }
        public int appFunctionID { get; set; }
        public HttpFileCollection files { get; set; }

        public int ucount { get; set; }
        public int dcount { get; set; }
        public int gcount { get; set; }
        public bool editable { get; set; }
    }
}