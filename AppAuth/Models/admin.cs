﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AppAuth.Models
{
    public class admin
    {
        [Key]
        public string account { get; set; }
        [Key]
        public int appID { get; set; }
        [Key]
        public int functionGroupID { get; set; }
        [Key]
        public int platformID { get; set; }
        [Key]
        public int functionID { get; set; }
        public bool isAdmin { get; set; }
        public int functionGroupIDBegin { get; set; }
        public int functionGroupIDEnd { get; set; }
    }
}