﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AppAuth.Models
{
    public class oneday_group_permission
    {
        public string groupID { get; set; }
        public int appID { get; set; }
        public int functionGroupID { get; set; }
        public int platformID { get; set; }
        public int functionID { get; set; }
        public int level { get; set; }
        public bool isEnabled { get; set; }
        public string comment { get; set; }
        [Key]
        public string uuid { get; set; }
    }
}