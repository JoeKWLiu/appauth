﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AppAuth.Models
{
    public class app_base
    {
        [Key]
        public int appID { get; set; }
        [Key]
        public int platformID { get; set; }
        public string name { get; set; }
    }
}