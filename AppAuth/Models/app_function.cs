﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppAuth.Models
{
    public class app_function
    {
        public int appID { get; set; }
        public int functionGroupID { get; set; }
        public int platformID { get; set; } = -1;
        public int functionID { get; set; }
        public string functionCode { get; set; }
        public string name { get; set; }
        public string comment { get; set; } = "";
        public bool defaultPermission { get; set; }
        public bool isEnabled { get; set; }
        public bool hasSubFunction { get; set; }
        public int sortOrder { get; set; }
        public string iconURL { get; set; } = "";
        public string webURL { get; set; } = "";
        public string uuid { get; set; }

        public HttpFileCollection files { get; set; }
        public int ucount { get; set; }
        public int dcount { get; set; }
        public int gcount { get; set; }
        public int scount { get; set; }
        public bool editable { get; set; }
    }
}