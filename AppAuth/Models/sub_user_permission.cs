﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppAuth.Models
{
    public class sub_user_permission : ICloneable
    {
        public string user { get; set; }
        public string mainUUID { get; set; }
        public int functionID { get; set; }
        public int level { get; set; }
        public bool isEnabled { get; set; }
        public string comment { get; set; }
        public string uuid { get; set; }

        public string name { get; set; }
        public int appID { get; set; }
        public int functionGroupID { get; set; }
        public int appFunctionID { get; set; }

        public string companyID { get; set; }
        public string departName { get; set; }
        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}