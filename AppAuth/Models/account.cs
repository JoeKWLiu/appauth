﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppAuth.Models
{
    public class account
    {
        public int aid { get; set; }
        public string Account { get; set; }
        public string name { get; set; }
        public string password { get; set; }
        public string password2 { get; set; }
        public int mainAid { get; set; }
        public string avatar { get; set; }
        public int type { get; set; }
        public int level { get; set; }
        public string companyID { get; set; }
        public string departID { get; set; }
        public string fullUnitID { get; set; }
        public string departName { get; set; }
        public string position { get; set; }
        public string extension { get; set; }
        public string ExtRealOrgId1 { get; set; }
        public string OrgSupport { get; set; }
        public bool isActive { get; set; }
        public bool isResign { get; set; }
        public bool appInstalled { get; set; }
        public bool isSuperUser { get; set; }
        public bool isSearchable { get; set; }
    }
}
