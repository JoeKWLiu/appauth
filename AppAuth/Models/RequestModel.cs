﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppAuth.Models
{
    public class RequestModel
    {
        private Guid _accessToken;

        public Guid accessToken {
            set {
                _accessToken = value;
                auth = new AuthModel() { accessToken = _accessToken };
            }
            get{
                return _accessToken;
            }
        }

        public AuthModel auth { get; private set; }

        public int appID { get; set; }
        public int functionGroupID { get; set; }
        public int plateformID { get; set; }
        public int functionID { get; set; }
        public int subFunctionID { get; set; }
        public Guid uuid { get; set; }
        public object sortList { get; set; }

        public string keywords { get; set; }
        public int menuID { get; set; }
    }
}