﻿using AppAuth.Models;
using AppAuth.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AppAuth.Controllers
{
    public class AccountController : ApiController
    {
        [HttpPost]
        public object getAccounts([FromBody]RequestModel request)
        {
            var acts = AccountRepository.getAccounts(request.keywords, 0);
            return Ok(acts);
        }

        [HttpPost]
        public object getAutoAccounts([FromBody]RequestModel request)
        {
            var acts = AccountRepository.getAutoAccounts(request.keywords, 5);
            return Ok(acts);
        }

        [HttpPost]
        public object getFullUnits([FromBody]RequestModel request)
        {
            var fus = AccountRepository.getFullUnits(request.keywords, 5);
            return Ok(fus);
        }
    }
}
