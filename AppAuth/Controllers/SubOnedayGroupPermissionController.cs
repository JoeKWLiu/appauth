﻿using AppAuth.Models;
using AppAuth.Repositories;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AppAuth.Controllers
{
    public class SubOnedayGroupPermissionController : ApiController
    {
        [HttpPost]
        public object getSubOnedayGroupPermissions([FromBody]RequestModel request)
        {
            var sdps = SubOnedayGroupPermissionRepository.getSubOnedayGroupPermissions()
                .Where(w => w.appID.Equals(request.appID) && w.functionGroupID.Equals(request.functionGroupID) && w.appFunctionID.Equals(request.functionID) && w.functionID.Equals(request.subFunctionID));

            return Ok(sdps);
        }
        [HttpPost]
        public object updateSubOnedayGroupPermissions([FromBody]RequestModel request)
        {
            string jstr = JsonConvert.SerializeObject(request.sortList);
            var sdpl = JsonConvert.DeserializeObject<List<sub_oneday_group_permission>>(jstr);

            SubOnedayGroupPermissionRepository.updateSubOnedayGroupPermissions(sdpl);
            return Ok(sdpl);
        }
        [HttpPost]
        public object insertSubOnedayGroupPermission([FromBody]RequestModel request)
        {
            string jstr = JsonConvert.SerializeObject(request.sortList);
            var sdp = JsonConvert.DeserializeObject<sub_oneday_group_permission>(jstr);

            var app = AppFunctionRepository.getAppFunctions().Where(w => w.appID.Equals(sdp.appID) && w.functionGroupID.Equals(sdp.functionGroupID) && w.functionID.Equals(sdp.appFunctionID)).FirstOrDefault();
            sdp.mainUUID = app.uuid;
            sdp.uuid = Guid.NewGuid().ToString();
            return Ok(SubOnedayGroupPermissionRepository.insertSubOnedayGroupPermission(sdp));
        }
        [HttpPost]
        public object deleteSubOnedayGroupPermission([FromBody]RequestModel request)
        {
            SubOnedayGroupPermissionRepository.deleteSubOnedayGroupPermission(request.uuid);
            return Ok(request.uuid);
        }
    }
}
