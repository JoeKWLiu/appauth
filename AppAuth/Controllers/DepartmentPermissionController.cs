﻿using AppAuth.Models;
using AppAuth.Repositories;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AppAuth.Controllers
{
    public class DepartmentPermissionController : ApiController
    {
        [HttpPost]
        public object getDepartmentPermissions([FromBody]RequestModel request)
        {
            var ups = DepartmentPermissionRepository.getDepartmentPermissions()
                        .Where(w => w.appID.Equals(request.appID) && w.functionGroupID.Equals(request.functionGroupID) && w.functionID.Equals(request.functionID));
            return Ok(ups);
        }
        [HttpPost]
        public object updateDepartmentPermissions([FromBody]RequestModel request)
        {
            string jstr = JsonConvert.SerializeObject(request.sortList);
            var dpl = JsonConvert.DeserializeObject<List<department_permission>>(jstr);

            DepartmentPermissionRepository.updateDepartmentPermissions(dpl);
            return Ok(dpl);
        }
        [HttpPost]
        public object insertDepartmentPermission([FromBody]RequestModel request)
        {
            string jstr = JsonConvert.SerializeObject(request.sortList);
            var dp = JsonConvert.DeserializeObject<department_permission>(jstr);

            return Ok(DepartmentPermissionRepository.insertDepartmentPermission(dp));
        }
        [HttpPost]
        public object deleteDepartmentPermission([FromBody]RequestModel request)
        {
            DepartmentPermissionRepository.deleteDepartmentPermission(request.uuid);
            return Ok(request.uuid);
        }
    }
}
