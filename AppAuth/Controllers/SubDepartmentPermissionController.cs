﻿using AppAuth.Models;
using AppAuth.Repositories;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AppAuth.Controllers
{
    public class SubDepartmentPermissionController : ApiController
    {
        [HttpPost]
        public object getSubDepartmentPermissions([FromBody]RequestModel request)
        {
            var sdps = SubDepartmentPermissionRepository.getSubDepartmentPermissions()
                .Where(w => w.appID.Equals(request.appID) && w.functionGroupID.Equals(request.functionGroupID) && w.appFunctionID.Equals(request.functionID) && w.functionID.Equals(request.subFunctionID));

            return Ok(sdps);
        }
        [HttpPost]
        public object updateSubDepartmentPermissions([FromBody]RequestModel request)
        {
            string jstr = JsonConvert.SerializeObject(request.sortList);
            var sdpl = JsonConvert.DeserializeObject<List<sub_department_permission>>(jstr);

            SubDepartmentPermissionRepository.updateSubDepartmentPermissions(sdpl);
            return Ok(sdpl);
        }
        [HttpPost]
        public object insertSubDepartmentPermission([FromBody]RequestModel request)
        {
            string jstr = JsonConvert.SerializeObject(request.sortList);
            var sdp = JsonConvert.DeserializeObject<sub_department_permission>(jstr);

            var app = AppFunctionRepository.getAppFunctions().Where(w => w.appID.Equals(sdp.appID) && w.functionGroupID.Equals(sdp.functionGroupID) && w.functionID.Equals(sdp.appFunctionID)).FirstOrDefault();
            sdp.mainUUID = app.uuid;

            return Ok(SubDepartmentPermissionRepository.insertSubDepartmentPermission(sdp));
        }
        [HttpPost]
        public object deleteSubDepartmentPermission([FromBody]RequestModel request)
        {
            SubDepartmentPermissionRepository.deleteSubDepartmentPermission(request.uuid);
            return Ok(request.uuid);
        }
    }
}
