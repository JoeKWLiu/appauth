﻿using AppAuth.Models;
using AppAuth.Repositories;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AppAuth.Controllers
{
    public class SubUserPermissionController : ApiController
    {
        [HttpPost]
        public object getSubUserPermissions([FromBody]RequestModel request)
        {
            var ups = SubUserPermissionRepository.getSubUserPermissions()
                        .Where(w => w.appID.Equals(request.appID) && w.functionGroupID.Equals(request.functionGroupID) && w.appFunctionID.Equals(request.functionID) && w.functionID.Equals(request.subFunctionID));
            return Ok(ups);
        }
        [HttpPost]
        public object updateSubUserPermissions([FromBody]RequestModel request)
        {
            string jstr = JsonConvert.SerializeObject(request.sortList);
            var supl = JsonConvert.DeserializeObject<List<sub_user_permission>>(jstr);

            return Ok(SubUserPermissionRepository.updateSubUserPermissions(supl));
        }
        [HttpPost]
        public object insertSubUserPermission([FromBody]RequestModel request)
        {
            string jstr = JsonConvert.SerializeObject(request.sortList);
            var sup = JsonConvert.DeserializeObject<sub_user_permission>(jstr);

            var app = AppFunctionRepository.getAppFunctions().Where(w => w.appID.Equals(sup.appID) && w.functionGroupID.Equals(sup.functionGroupID) && w.functionID.Equals(sup.appFunctionID)).FirstOrDefault();
            sup.mainUUID = app.uuid;

            return Ok(SubUserPermissionRepository.insertSubUserPermission(sup));
        }
        [HttpPost]
        public object deleteSubUserPermission([FromBody]RequestModel request)
        {
            SubUserPermissionRepository.deleteSubUserPermission(request.uuid);
            return Ok(request.uuid);
        }
    }
}
