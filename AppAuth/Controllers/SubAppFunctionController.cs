﻿using AppAuth.Filter;
using AppAuth.Models;
using AppAuth.Repositories;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Web;
using System.Web.Http;

namespace AppAuth.Controllers
{
    public class SubAppFunctionController : ApiController
    {
        [HttpPost]
        public object getSubAppFunctions([FromBody]RequestModel request)
        {
            object result = null;
            IEnumerable<sub_app_function> afs = SubAppFunctionRepository.getSubAppFunctions()
                                                    .Where(w => w.appID.Equals(request.appID) && w.functionGroupID.Equals(request.functionGroupID) && w.appFunctionID.Equals(request.functionID));

            var privs = request.auth.privilege.Where(w => w.appID.Equals(request.appID) || w.appID.Equals(0));

            IEnumerable<sub_app_function> have_privs_sfunc = Enumerable.Empty<sub_app_function>();
            foreach (var priv in privs)
            {
                if (priv.functionGroupID.Equals(0))
                {
                    if ((priv.functionGroupIDBegin + priv.functionGroupIDEnd).Equals(0))
                    {
                        if (priv.functionID.Equals(0))
                        {
                            have_privs_sfunc = have_privs_sfunc.Union(afs);
                        }
                        else
                        {
                            have_privs_sfunc = have_privs_sfunc.Union(afs.Where(w => w.appFunctionID.Equals(priv.functionID)));
                        }
                    }
                    else
                    {
                        if (priv.functionID.Equals(0))
                        {
                            have_privs_sfunc = have_privs_sfunc.Union(afs.Where(w => w.functionGroupID >= priv.functionGroupIDBegin && w.functionGroupID <= priv.functionGroupIDEnd));
                        }
                        else
                        {
                            have_privs_sfunc = have_privs_sfunc.Union(afs.Where(w => w.functionGroupID >= priv.functionGroupIDBegin && w.functionGroupID <= priv.functionGroupIDEnd && w.appFunctionID.Equals(priv.functionID)));
                        }
                    }
                }
                else
                {
                    if (priv.functionID.Equals(0))
                    {
                        have_privs_sfunc = have_privs_sfunc.Union(afs.Where(w => w.functionGroupID.Equals(priv.functionGroupID)));
                    }
                    else
                    {
                        have_privs_sfunc = have_privs_sfunc.Union(afs.Where(w => w.functionGroupID.Equals(priv.functionGroupID) && w.appFunctionID.Equals(priv.functionID)));
                    }
                }
            }

            foreach (sub_app_function hpf in have_privs_sfunc)
            {
                hpf.editable = privs.Any(a => (a.functionGroupID.Equals(hpf.functionGroupID) || (a.functionGroupID.Equals(0) && (hpf.functionGroupID >= a.functionGroupIDBegin && hpf.functionGroupID <= a.functionGroupIDEnd))) && (a.functionID.Equals(0) || a.functionID.Equals(hpf.appFunctionID)) && a.isAdmin);

                if (privs.Any(a => a.functionGroupID.Equals(0) && a.functionID.Equals(0) && a.functionGroupIDBegin.Equals(0) && a.functionGroupIDEnd.Equals(0) && a.isAdmin))
                {
                    hpf.editable = true;
                }
            }

            result = new
            {
                isMovable = privs.Where(w => w.isAdmin && (
                                w.functionID.Equals(0) || w.functionID.Equals(request.functionID)
                            ) && (
                                w.functionGroupID.Equals(request.functionGroupID) ||
                                (request.functionGroupID >= w.functionGroupIDBegin && request.functionGroupID <= w.functionGroupIDEnd) ||
                                (w.functionGroupID + w.functionGroupIDBegin + w.functionGroupIDEnd).Equals(0)
                            )).Any(),
                data = have_privs_sfunc.OrderBy(o => o.sortOrder)
            };

            return Ok(result);
        }

        [IgnoreResultAttribute]
        [HttpPost]
        public object updateSubAppFunctions()
        {
            log lg = new log();
            List<sub_app_function> safl = new List<sub_app_function>();
            try
            {
                var rq = HttpContext.Current.Request;
                safl = JsonConvert.DeserializeObject<List<sub_app_function>>(rq["sortList"]);

                string controller = this.GetType().Name.Replace("Controller", String.Empty);
                string action = MethodBase.GetCurrentMethod().Name;
                RequestModel rm = new RequestModel();
                rm.accessToken = new Guid(rq["accessToken"]);
                lg.CreatedBy = rm.auth.token.data.account;
                lg.Controller = controller;
                lg.Action = action;
                lg.Param = JsonConvert.SerializeObject(safl);
                lg.Status = HttpStatusCode.OK.ToString();
                lg.Msg = String.Empty;
                lg.CreatedOn = DateTime.Now;

                //  單筆更新，處理檔案上傳
                if (safl.Count().Equals(1) && rq.Files.Count.Equals(1))
                {
                    var fileName = Guid.NewGuid().ToString();

                    safl[0].files = rq.Files;
                    var exttension = System.IO.Path.GetExtension(safl[0].files[0].FileName);
                    var filePath = HttpContext.Current.Server.MapPath("~/icons/sub_app_function/" + fileName + exttension);
                    safl[0].files[0].SaveAs(filePath);

                    //var old_filePath = HttpContext.Current.Server.MapPath($"~/icons/sub_app_function/{Path.GetFileName(safl[0].iconURL)}");
                    //if (File.Exists(old_filePath))
                    //{
                    //    File.Delete(old_filePath);
                    //}

                    var iconPath = SubAppFunctionRepository.getIconPath();
                    safl[0].iconURL = $"{iconPath}{fileName}{exttension}";
                    //safl[0].iconURL = $"https://appcloud.fpcetg.com.tw/apppem/icons/sub_app_function/{fileName}{exttension}";
                    //safl[0].iconURL = $"/icons/sub_app_function/{fileName}{exttension}";
                }

                // 取得 同appID, functionGroupID, functionID 所有資料
                IEnumerable<sub_app_function> safs = SubAppFunctionRepository.getSubAppFunctions()
                                            .Where(w => w.appID.Equals(safl.FirstOrDefault().appID) && w.functionGroupID.Equals(safl.FirstOrDefault().functionGroupID) && w.appFunctionID.Equals(safl.FirstOrDefault().functionID));
                int max_seq = safs.OrderByDescending(o => o.sortOrder).Select(s => s.sortOrder).FirstOrDefault() + 1;

                List<int> position = safl.Select(s => s.sortOrder).OrderBy(o => o).ToList();
                for (int i = 0; i < position.Count(); i++)
                {
                    if (position[i].Equals(0))
                    {
                        position[i] = max_seq;
                        max_seq++;
                    }
                }

                for (int i = 0; i < safl.Count(); i++)
                {
                    //fgbl[i].sortOrder = i + 1;
                    safl[i].sortOrder = position[i];
                }

                SubAppFunctionRepository.updateSubAppFunctions(safl);

                return Ok(safl);
            }
            catch (Exception ex)
            {
                lg.Status = HttpStatusCode.InternalServerError.ToString();
                lg.Msg = ex.Message;
                return InternalServerError();
            }
            finally
            {
                LogRepository.Log(lg);
            }
        }

        [IgnoreResultAttribute]
        [HttpPost]
        public object addSubAppFunction()
        {
            log lg = new log();
            sub_app_function saf = new sub_app_function();
            try
            {
                var rq = HttpContext.Current.Request;
                saf = JsonConvert.DeserializeObject<sub_app_function>(rq["sortList"]);

                string controller = this.GetType().Name.Replace("Controller", String.Empty);
                string action = MethodBase.GetCurrentMethod().Name;
                RequestModel rm = new RequestModel();
                rm.accessToken = new Guid(rq["accessToken"]);
                lg.CreatedBy = rm.auth.token.data.account;
                lg.Controller = controller;
                lg.Action = action;
                lg.Param = JsonConvert.SerializeObject(saf);
                lg.Status = HttpStatusCode.OK.ToString();
                lg.Msg = String.Empty;
                lg.CreatedOn = DateTime.Now;

                saf.uuid = Guid.NewGuid().ToString();
                saf.iconURL = saf.iconURL ?? "";
                saf.webURL = saf.webURL ?? "";

                if (rq.Files.Count > 0)
                {
                    var fileName = Guid.NewGuid().ToString();

                    saf.files = rq.Files;
                    var exttension = System.IO.Path.GetExtension(saf.files[0].FileName);
                    var filePath = HttpContext.Current.Server.MapPath("~/icons/sub_app_function/" + fileName + exttension);
                    saf.files[0].SaveAs(filePath);

                    var iconPath = SubAppFunctionRepository.getIconPath();
                    saf.iconURL = $"{iconPath}{fileName}{exttension}";
                    //af.iconURL = $"https://appcloud.fpcetg.com.tw/apppem/icons/app_function/{af.uuid}{exttension}";
                    //saf.iconURL = $"/icons/app_function/{fileName}{exttension}";
                }

                var app = AppFunctionRepository.getAppFunctions().Where(w => w.appID.Equals(saf.appID) && w.functionGroupID.Equals(saf.functionGroupID) && w.functionID.Equals(saf.appFunctionID)).FirstOrDefault();
                saf.mainUUID = app.uuid;

                // 取得 同appID, functionGroupID , functionID 所有資料
                IEnumerable<sub_app_function> safs = SubAppFunctionRepository.getSubAppFunctions()
                                            .Where(w => w.appID.Equals(saf.appID) && w.functionGroupID.Equals(saf.functionGroupID) && w.appFunctionID.Equals(saf.appFunctionID));
                int max_seq = safs.OrderByDescending(o => o.sortOrder).Select(s => s.sortOrder).FirstOrDefault() + 1;
                int max_functionID = safs.OrderByDescending(o => o.functionID).Select(s => s.functionID).FirstOrDefault() + 1;
                saf.sortOrder = max_seq;
                saf.functionID = max_functionID;

                saf.functionCode = $"APPID_{saf.appID}_GROUP_{saf.functionGroupID}_FUNCTION_{saf.appFunctionID}_SUBFUNCTION_{saf.functionID}";
                SubAppFunctionRepository.insertSubAppFunction(saf);
                return Ok(saf);
            }
            catch (Exception ex)
            {
                lg.Status = HttpStatusCode.InternalServerError.ToString();
                lg.Msg = ex.Message;
                return InternalServerError();
            }
            finally
            {
                LogRepository.Log(lg);
            }
        }

        [HttpPost]
        public object deleteSubAppFunction([FromBody]RequestModel request)
        {
            SubAppFunctionRepository.deleteSubAppFunction(request.uuid);
            return Ok(request.uuid);
        }
    }
}
