﻿using AppAuth.Models;
using AppAuth.Repositories;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AppAuth.Controllers
{
    public class FunctionGroupBaseController : ApiController
    {
        [HttpPost]
        public object getFunctionGroupBases([FromBody]RequestModel request)
        {
            object result = null;
            IEnumerable<function_group_base> fgbs = FunctionGroupBaseRepository.getFunctionGroupBases()
                                                    .Where(w => w.appID.Equals(request.appID));
            var privs = request.auth.privilege.Where(w => w.appID.Equals(request.appID) || w.appID.Equals(0));

            IEnumerable<function_group_base> have_privs_group = Enumerable.Empty<function_group_base>();
            foreach (var priv in privs)
            {
                if (priv.functionGroupID.Equals(0))
                {
                    if((priv.functionGroupIDBegin + priv.functionGroupIDEnd).Equals(0))
                    {
                        have_privs_group = have_privs_group.Union(fgbs);
                    }
                    else
                    {
                        have_privs_group = have_privs_group.Union(fgbs.Where(w => w.functionGroupID >= priv.functionGroupIDBegin && w.functionGroupID <= priv.functionGroupIDEnd));
                    }
                }
                else
                {
                    have_privs_group = have_privs_group.Union(fgbs.Where(w => w.functionGroupID.Equals(priv.functionGroupID)));
                }
            }

            foreach(function_group_base hpg in have_privs_group)
            {
                hpg.editable = privs.Any(a => (a.functionGroupID.Equals(hpg.functionGroupID) || (hpg.functionGroupID >= a.functionGroupIDBegin && hpg.functionGroupID <= a.functionGroupIDEnd)) && a.functionID.Equals(0) && a.isAdmin);

                if (privs.Any(a => a.functionGroupID.Equals(0) && a.functionGroupIDBegin.Equals(0) && a.functionGroupIDEnd.Equals(0) && a.isAdmin))
                {
                    hpg.editable = true;
                }
            }

            result = new
            {
                isMovable = privs.Where(w => w.functionGroupID.Equals(0) && w.functionID.Equals(0) && w.isAdmin).Any(),
                data = have_privs_group.OrderBy(o => o.sortOrder)
            };
            return Ok(result);
        }

        [HttpPost]
        public object updateFunctionGroupBases([FromBody]RequestModel request)
        {
            string jstr = JsonConvert.SerializeObject(request.sortList);
            var fgbl = JsonConvert.DeserializeObject<List<function_group_base>>(jstr);

            List<int> position = fgbl.Select(s => s.sortOrder).OrderBy(o => o).ToList();

            for (int i = 0; i < fgbl.Count(); i++)
            {
                //fgbl[i].sortOrder = i + 1;
                fgbl[i].sortOrder = position[i];
            }

            FunctionGroupBaseRepository.updateFunctionGroupBases(fgbl);

            return Ok(fgbl);
        }

        [HttpPost]
        public object addFunctionGroupBase([FromBody]RequestModel request)
        {
            string jstr = JsonConvert.SerializeObject(request.sortList);
            var fgb = JsonConvert.DeserializeObject<function_group_base>(jstr);
            var privs = request.auth.privilege.Where(w => w.appID.Equals(request.appID) || w.appID.Equals(0));

            IEnumerable<function_group_base> fgbsIndb = FunctionGroupBaseRepository.getFunctionGroupBases()
                                                    .Where(w => w.appID.Equals(fgb.appID)).OrderBy(o => o.functionGroupID);
            // 將第一階過濾存起來
            var fgbsIndb_org = fgbsIndb;

            fgb.sortOrder = fgbsIndb.Count() + 1;

            var add_privs = privs.Where(w => w.functionGroupID.Equals(0) && w.functionID.Equals(0) && w.isAdmin).OrderBy(o => o.functionGroupIDBegin);


            if (privs.Where(w => w.functionGroupID.Equals(0) && w.functionGroupIDBegin.Equals(0) && w.functionGroupIDEnd.Equals(0)).Any())
            {
                int tempCount = 0;
                foreach (var fgbIndb in fgbsIndb)
                {
                    if (fgbIndb.functionGroupID - 1 == tempCount)
                    {
                        tempCount = fgbIndb.functionGroupID;
                    }
                    else
                    {
                        fgb.functionGroupID = tempCount + 1;
                        break;
                    }
                }

                if (fgb.functionGroupID.Equals(0))
                {
                    fgb.functionGroupID = tempCount + 1;
                }
            }
            else
            {
                foreach (var priv in add_privs.OrderBy(o => o.functionGroupIDBegin))
                {
                    var pfg = fgbsIndb.Where(w => w.functionGroupID >= priv.functionGroupIDBegin && w.functionGroupID <= priv.functionGroupIDEnd);
                    if (pfg.Count() < (priv.functionGroupIDEnd - priv.functionGroupIDBegin + 1))
                    {
                        if (pfg.Count().Equals(0))
                        {
                            fgb.functionGroupID = priv.functionGroupIDBegin;
                        }
                        else
                        {
                            fgb.functionGroupID = pfg.Max(m => m.functionGroupID) + 1;
                        }
                        break;
                        //int tempCount = 0;
                        //foreach (var fgbIndb in fgbsIndb)
                        //{
                        //    if (fgbIndb.functionGroupID - 1 == tempCount)
                        //    {
                        //        tempCount = fgbIndb.functionGroupID;
                        //    }
                        //    else
                        //    {
                        //        fgb.functionGroupID = tempCount + 1;
                        //        break;
                        //    }
                        //}

                        //if (fgb.functionGroupID.Equals(0))
                        //{
                        //    fgb.functionGroupID = tempCount + 1;
                        //}
                    }
                }
            }

            fgb.functionGroupCode = $"APPID_{fgb.appID}_GROUP_{fgb.functionGroupID}";

            FunctionGroupBaseRepository.insertFunctionGroupBase(fgb);
            fgb.uuid = FunctionGroupBaseRepository.getFunctionGroupBases().Where(w => w.appID.Equals(fgb.appID) && w.functionGroupID.Equals(fgb.functionGroupID)).FirstOrDefault().uuid;

            return Ok(fgb);
        }

        [HttpPost]
        public object deleteFunctionGroupBase([FromBody]RequestModel request)
        {
            FunctionGroupBaseRepository.deleteFunctionGroupBase(request.uuid);
            return Ok(request.uuid);
        }
    }
}
