﻿using AppAuth.Models;
using AppAuth.Repositories;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AppAuth.Controllers
{
    public class OnedayGroupPermissionController : ApiController
    {
        [HttpPost]
        public object getOnedayGroupPermissions([FromBody]RequestModel request)
        {
            var ups = OnedayGroupPermissionRepository.getOnedayGroupPermissions()
                        .Where(w => w.appID.Equals(request.appID) && w.functionGroupID.Equals(request.functionGroupID) && w.functionID.Equals(request.functionID));
            return Ok(ups);
        }
        [HttpPost]
        public object updateOnedayGroupPermissions([FromBody]RequestModel request)
        {
            string jstr = JsonConvert.SerializeObject(request.sortList);
            var dpl = JsonConvert.DeserializeObject<List<oneday_group_permission>>(jstr);

            OnedayGroupPermissionRepository.updateOnedayGroupPermissions(dpl);
            return Ok(dpl);
        }
        [HttpPost]
        public object insertOnedayGroupPermission([FromBody]RequestModel request)
        {
            string jstr = JsonConvert.SerializeObject(request.sortList);
            var dp = JsonConvert.DeserializeObject<oneday_group_permission>(jstr);
            dp.uuid = Guid.NewGuid().ToString();
            return Ok(OnedayGroupPermissionRepository.insertOnedayGroupPermission(dp));
        }
        [HttpPost]
        public object deleteOnedayGroupPermission([FromBody]RequestModel request)
        {
            OnedayGroupPermissionRepository.deleteOnedayGroupPermission(request.uuid);
            return Ok(request.uuid);
        }
    }
}
