﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Query;
using System.Web.Http.OData.Routing;
using AppAuth.Models;
using AppAuth.Repositories;
using AppAuth.Services;
using Microsoft.Data.OData;

namespace AppAuth.Controllers
{
    public class AppBaseController : ApiController
    {

        [HttpPost]
        public object getAppBases([FromBody]RequestModel request)
        {
            var appIDs = request.auth.privilege.Select(s => s.appID).Distinct();
            var result = AppBaseRepository.getAppBases(appIDs).Where(w => w.platformID.Equals(1));
            if (!request.appID.Equals(0))
            {
                result = result.Where(w => w.appID.Equals(request.appID));
            }
            return Ok(result);
        }
    }
}
