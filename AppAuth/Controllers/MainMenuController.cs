﻿using AppAuth.Models;
using AppAuth.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AppAuth.Controllers
{
    public class MainMenuController : ApiController
    {

        [HttpPost]
        public object getMainMenuByAccount([FromBody]RequestModel request)
        {
            if (request.appID > 2)
            {
                return null;
            }
            return MainMenuRepository.getMenu(request.appID, request.menuID, request.keywords, request.uuid);
        }

        [HttpPost]
        public object getDemoUserPermissions([FromBody]RequestModel request)
        {
            // 用 app, user 找權限
            var ups = UserPermissionRepository.getUserPermissions()
            .Where(w => (w.appID.Equals(request.appID) || w.appID.Equals(0)) && w.user.Equals(request.keywords))
            .ToList();

            // app 清單
            var apps = AppBaseRepository.getAppBases(new List<int>() { 0 });

            // group 清單
            var fgs = FunctionGroupBaseRepository.getFunctionGroupBases();
            var isGroup0 = ups.Where(w => w.functionGroupID.Equals(0)).Any();

            // group 處理
            if (isGroup0)
            {
                List<user_permission> addList = new List<user_permission>();

                var group_0s = ups.Where(w => w.functionGroupID.Equals(0));
                foreach (var group_0 in group_0s)
                {
                    foreach (var fg in fgs.Where(w => w.appID.Equals(group_0.appID)))
                    {
                        var tmpg = (user_permission)group_0.Clone();
                        tmpg.functionGroupID = fg.functionGroupID;
                        addList.Add(tmpg);
                    }
                }
                ups.AddRange(addList);
                ups = ups.Where(w => !w.functionGroupID.Equals(0)).ToList();
            }

            // function 清單
            var afs = AppFunctionRepository.getAppFunctions();
            var isFunc0 = ups.Where(w => w.functionID.Equals(0)).Any();

            // function 處理
            if (isFunc0)
            {
                List<user_permission> addList = new List<user_permission>();
                var func_0s = ups.Where(w => w.functionID.Equals(0));
                foreach (var func_0 in func_0s)
                {
                    foreach (var af in afs.Where(w => w.appID.Equals(func_0.appID) && w.functionGroupID.Equals(func_0.functionGroupID)))
                    {
                        var tmpf = (user_permission)func_0.Clone();
                        tmpf.functionID = af.functionID;
                        addList.Add(tmpf);
                    }
                }
                ups.AddRange(addList);
                ups = ups.Where(w => !w.functionID.Equals(0)).ToList();
            }

            // sub function 清單
            var sups = SubUserPermissionRepository.getSubUserPermissions()
                .Where(w => w.user.Equals(request.keywords) && w.appID.Equals(request.appID))
                .ToList();

            var safs = SubAppFunctionRepository.getSubAppFunctions();
            var isSubFunc0 = sups.Where(w => w.functionID.Equals(0)).Any();

            // sub function 處理
            if (isSubFunc0)
            {
                List<sub_user_permission> addList = new List<sub_user_permission>();
                var subfunc_0s = sups.Where(w => w.functionID.Equals(0));
                foreach (var subfunc_0 in subfunc_0s)
                {
                    foreach (var saf in safs.Where(w => w.mainUUID.Equals(subfunc_0.mainUUID)))
                    {
                        var tmpsf = (sub_user_permission)subfunc_0.Clone();
                        tmpsf.functionID = saf.functionID;
                        sups.Add(tmpsf);
                    }
                }
                sups.AddRange(addList);
                sups = sups.Where(w => !w.functionID.Equals(0)).ToList();
            }

            var result =
                new
                {
                    app = apps.Where(w => w.appID.Equals(request.appID) && w.platformID.Equals(1)),
                    group_list = from up in ups
                                 group up by new { up.appID, up.functionGroupID } into upg
                                 select new
                                 {
                                     function_group = fgs.Where(w => w.appID.Equals(upg.Key.appID) && w.functionGroupID.Equals(upg.Key.functionGroupID)),
                                     function_list = from af in upg
                                                     group af by new { af.appID, af.functionGroupID, af.functionID } into afg
                                                     select new
                                                     {
                                                         app_function = afs.Where(w => w.appID.Equals(afg.Key.appID) && w.functionGroupID.Equals(afg.Key.functionGroupID) && w.functionID.Equals(afg.Key.functionID)),
                                                         sub_function_list = from saf in afg
                                                                             join sup in sups on
                                                                              new { saf.appID, saf.functionGroupID, saf.functionID } equals new { sup.appID, sup.functionGroupID, functionID = sup.appFunctionID }
                                                                             select new
                                                                             {
                                                                                 sub_app_function = safs.Where(w => w.mainUUID.Equals(sup.mainUUID))
                                                                             }
                                                     }
                                 }
                }
;


            return Ok(result);
        }
    }
}
