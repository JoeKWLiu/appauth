﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Query;
using System.Web.Http.OData.Routing;
using AppAuth.Models;
using AppAuth.Repositories;
using Microsoft.Data.OData;
using Newtonsoft.Json;

namespace AppAuth.Controllers
{
    public class UserPermissionController : ApiController
    {
        [HttpPost]
        public object getUserPermissions([FromBody]RequestModel request)
        {
            var ups = UserPermissionRepository.getUserPermissions()
                        .Where(w => w.appID.Equals(request.appID) && w.functionGroupID.Equals(request.functionGroupID) && w.functionID.Equals(request.functionID));
            return Ok(ups);
        }
        [HttpPost]
        public object updateUserPermissions([FromBody]RequestModel request)
        {
            string jstr = JsonConvert.SerializeObject(request.sortList);
            var upl = JsonConvert.DeserializeObject<List<user_permission>>(jstr);

            return Ok(UserPermissionRepository.updateUserPermissions(upl));
        }
        [HttpPost]
        public object insertUserPermission([FromBody]RequestModel request)
        {
            string jstr = JsonConvert.SerializeObject(request.sortList);
            var up = JsonConvert.DeserializeObject<user_permission>(jstr);

            return Ok(UserPermissionRepository.insertUserPermission(up));
        }
        [HttpPost]
        public object deleteUserPermission([FromBody]RequestModel request)
        {
            UserPermissionRepository.deleteUserPermission(request.uuid);
            return Ok(request.uuid);
        }

    }
}
