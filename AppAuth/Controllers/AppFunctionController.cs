﻿using AppAuth.Filter;
using AppAuth.Models;
using AppAuth.Repositories;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Web;
using System.Web.Http;

namespace AppAuth.Controllers
{
    public class AppFunctionController : ApiController
    {
        [HttpPost]
        public object getAppFunctions([FromBody]RequestModel request)
        {
            object result = null;
            IEnumerable<app_function> afs = AppFunctionRepository.getAppFunctions()
                                                    .Where(w => w.appID.Equals(request.appID) && w.functionGroupID.Equals(request.functionGroupID));

            var privs = request.auth.privilege.Where(w => w.appID.Equals(request.appID) || w.appID.Equals(0));

            IEnumerable<app_function> have_privs_func = Enumerable.Empty<app_function>();
            foreach (var priv in privs)
            {
                if (priv.functionGroupID.Equals(0))
                {
                    if ((priv.functionGroupIDBegin + priv.functionGroupIDEnd).Equals(0))
                    {
                        if (priv.functionID.Equals(0))
                        {
                            have_privs_func = have_privs_func.Union(afs);
                        }
                        else
                        {
                            have_privs_func = have_privs_func.Union(afs.Where(w => w.functionID.Equals(priv.functionID)));
                        }
                    }
                    else
                    {
                        if (priv.functionID.Equals(0))
                        {
                            have_privs_func = have_privs_func.Union(afs.Where(w => w.functionGroupID >= priv.functionGroupIDBegin && w.functionGroupID <= priv.functionGroupIDEnd));
                        }
                        else
                        {
                            have_privs_func = have_privs_func.Union(afs.Where(w => w.functionGroupID >= priv.functionGroupIDBegin && w.functionGroupID <= priv.functionGroupIDEnd && w.functionID.Equals(priv.functionID)));
                        }
                    }
                }
                else
                {
                    if (priv.functionID.Equals(0))
                    {
                        have_privs_func = have_privs_func.Union(afs.Where(w => w.functionGroupID.Equals(priv.functionGroupID)));
                    }
                    else
                    {
                        have_privs_func = have_privs_func.Union(afs.Where(w => w.functionGroupID.Equals(priv.functionGroupID) && w.functionID.Equals(priv.functionID)));
                    }
                }
            }

            foreach (app_function hpf in have_privs_func)
            {
                hpf.editable = privs.Any(a => (a.functionGroupID.Equals(hpf.functionGroupID) || (a.functionGroupID.Equals(0) && (hpf.functionGroupID >= a.functionGroupIDBegin && hpf.functionGroupID <= a.functionGroupIDEnd))) && (a.functionID.Equals(0) || a.functionID.Equals(hpf.functionID)) && a.isAdmin);

                if (privs.Any(a => a.functionGroupID.Equals(0) && a.functionID.Equals(0) && a.functionGroupIDBegin.Equals(0) && a.functionGroupIDEnd.Equals(0) && a.isAdmin))
                {
                    hpf.editable = true;
                }
            }

            result = new
            {
                isMovable = privs.Where(w => w.isAdmin && w.functionID.Equals(0) && (
                            w.functionGroupID.Equals(request.functionGroupID) || 
                            (request.functionGroupID >= w.functionGroupIDBegin && request.functionGroupID <= w.functionGroupIDEnd) ||
                            (w.functionGroupID + w.functionGroupIDBegin + w.functionGroupIDEnd).Equals(0)
                            )).Any(),
                data = have_privs_func.OrderBy(o => o.sortOrder)
            };
            return Ok(result);
        }

        [IgnoreResultAttribute]
        [HttpPost]
        public object updateAppFunctions()
        {
            log lg = new log();
            List<app_function> afl = new List<app_function>();
            try
            {
                var rq = HttpContext.Current.Request;
                afl = JsonConvert.DeserializeObject<List<app_function>>(rq["sortList"]);

                string controller = this.GetType().Name.Replace("Controller", String.Empty);
                string action = MethodBase.GetCurrentMethod().Name;
                RequestModel rm = new RequestModel();
                rm.accessToken = new Guid(rq["accessToken"]);
                lg.CreatedBy = rm.auth.token.data.account;
                lg.Controller = controller;
                lg.Action = action;
                lg.Param = JsonConvert.SerializeObject(afl);
                lg.Status = HttpStatusCode.OK.ToString();
                lg.Msg = String.Empty;
                lg.CreatedOn = DateTime.Now;

                //  單筆更新，處理檔案上傳
                if (afl.Count().Equals(1) && rq.Files.Count.Equals(1))
                {
                    var fileName = Guid.NewGuid().ToString();

                    afl[0].files = rq.Files;
                    var exttension = System.IO.Path.GetExtension(afl[0].files[0].FileName);
                    var filePath = HttpContext.Current.Server.MapPath("~/icons/app_function/" + fileName + exttension);
                    afl[0].files[0].SaveAs(filePath);

                    //var old_filePath = HttpContext.Current.Server.MapPath($"~/icons/app_function/{Path.GetFileName(afl[0].iconURL)}");
                    //if (File.Exists(old_filePath))
                    //{
                    //    File.Delete(old_filePath);
                    //}

                    var iconPath = AppFunctionRepository.getIconPath();
                    afl[0].iconURL = $"{iconPath}{fileName}{exttension}";
                    //afl[0].iconURL = $"/icons/app_function/{fileName}{exttension}";
                }

                // 取得 同appID, functionGroupID 所有資料
                IEnumerable<app_function> afs = AppFunctionRepository.getAppFunctions()
                                            .Where(w => w.appID.Equals(afl.FirstOrDefault().appID) && w.functionGroupID.Equals(afl.FirstOrDefault().functionGroupID));
                int max_seq = afs.OrderByDescending(o => o.sortOrder).Select(s => s.sortOrder).FirstOrDefault() + 1;

                List<int> position = afl.Select(s => s.sortOrder).OrderBy(o => o).ToList();
                for (int i = 0; i < position.Count(); i++)
                {
                    if (position[i].Equals(0))
                    {
                        position[i] = max_seq;
                        max_seq++;
                    }
                }

                for (int i = 0; i < afl.Count(); i++)
                {
                    //fgbl[i].sortOrder = i + 1;
                    afl[i].sortOrder = position[i];
                }

                AppFunctionRepository.updateAppFunctions(afl);

                return Ok(afl);
            }
            catch (Exception ex)
            {
                lg.Status = HttpStatusCode.InternalServerError.ToString();
                lg.Msg = ex.Message;
                return InternalServerError();
            }
            finally
            {
                LogRepository.Log(lg);
            }
        }

        [IgnoreResultAttribute]
        [HttpPost]
        public object addAppFunction()
        {
            log lg = new log();
            app_function af = new app_function();
            try
            {
                var rq = HttpContext.Current.Request;
                af = JsonConvert.DeserializeObject<app_function>(rq["sortList"]);

                string controller = this.GetType().Name.Replace("Controller", String.Empty);
                string action = MethodBase.GetCurrentMethod().Name;
                RequestModel rm = new RequestModel();
                rm.accessToken = new Guid(rq["accessToken"]);
                lg.CreatedBy = rm.auth.token.data.account;
                lg.Controller = controller;
                lg.Action = action;
                lg.Param = JsonConvert.SerializeObject(af);
                lg.Status = HttpStatusCode.OK.ToString();
                lg.Msg = String.Empty;
                lg.CreatedOn = DateTime.Now;

                af.uuid = Guid.NewGuid().ToString();

                if (rq.Files.Count > 0)
                {
                    var fileName = Guid.NewGuid().ToString();

                    af.files = rq.Files;
                    var exttension = System.IO.Path.GetExtension(af.files[0].FileName);

                    var filePath = HttpContext.Current.Server.MapPath("~/icons/app_function/" + fileName + exttension);
                    af.files[0].SaveAs(filePath);

                    var iconPath = AppFunctionRepository.getIconPath();
                    af.iconURL = $"{iconPath}{fileName}{exttension}";
                    //af.iconURL = $"/icons/app_function/{fileName}{exttension}";
                }

                // 取得 同appID, functionGroupID 所有資料
                IEnumerable<app_function> afs = AppFunctionRepository.getAppFunctions()
                                            .Where(w => w.appID.Equals(af.appID) && w.functionGroupID.Equals(af.functionGroupID));
                int max_seq = afs.OrderByDescending(o => o.sortOrder).Select(s => s.sortOrder).FirstOrDefault() + 1;
                int max_functionID = afs.OrderByDescending(o => o.functionID).Select(s => s.functionID).FirstOrDefault() + 1;
                af.sortOrder = max_seq;
                af.functionID = max_functionID;

                af.functionCode = $"APPID_{af.appID}_GROUP_{af.functionGroupID}_FUNCTION_{af.functionID}";
                AppFunctionRepository.insertAppFunction(af);

                return Ok(af);
            }
            catch(Exception ex)
            {
                lg.Status = HttpStatusCode.InternalServerError.ToString();
                lg.Msg = ex.Message;
                return InternalServerError();
            }
            finally
            {
                LogRepository.Log(lg);
            }
        }

        [HttpPost]
        public object deleteAppFunction([FromBody]RequestModel request)
        {
            AppFunctionRepository.deleteAppFunction(request.uuid);
            return Ok(request.uuid);
        }
    }
}
