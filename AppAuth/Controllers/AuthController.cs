﻿using AppAuth.Models;
using AppAuth.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AppAuth.Controllers
{
    public class AuthController : ApiController
    {
        [HttpPost]
        public object Login([FromBody]LoginRequest loginInfo)
        {
            return LoginService.Login(loginInfo);
        }

        [HttpPost]
        public bool isTest([FromBody]RequestModel request)
        {
            return DbService.isTest();
        }
    }
}
