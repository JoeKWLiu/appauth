﻿using AppAuth.Models;
using AppAuth.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AppAuth.Controllers
{
    public class MlFpcReadController : ApiController
    {
        [HttpPost]
        public object getViewGroup([FromBody]RequestModel request)
        {
            var acts = MlFpcReadRepository.getViewGroup(request.auth.token.data.account);
            return Ok(acts);
        }

        [HttpPost]
        public object getGroupCount([FromBody]RequestModel request)
        {
            var acts = MlFpcReadRepository.getGroupCount();
            return Ok(acts);
        }
    }
}
