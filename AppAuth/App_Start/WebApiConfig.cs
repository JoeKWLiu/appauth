﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.OData.Builder;
using System.Web.Http.OData.Extensions;
using AppAuth.Models;

namespace AppAuth
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API 設定和服務

            // Web API 路由
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{action}",
                defaults: new { action = RouteParameter.Optional }
            );

            config.Filters.Add(new Filter.AuthorizeAttribute());
            config.Filters.Add(new Filter.ResultAttribute());
            config.Filters.Add(new Filter.ExceptionAttribute());

            ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
            builder.EntitySet<user_permission>("UserPermission");
            builder.EntitySet<app_base>("AppBase");
            config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
        }
    }
}
