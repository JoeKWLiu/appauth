(function($){
    $(function () {
        $('.modal').modal();
        
        $('.preloader-background').fadeOut();
    }); // end of document ready
})(jQuery); // end of jQuery name space

//	Babel
function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }
function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

//toBlob polyfill
if (!HTMLCanvasElement.prototype.toBlob) {
    Object.defineProperty(HTMLCanvasElement.prototype, 'toBlob', {
        value: function (callback, type, quality) {
            var dataURL = this.toDataURL(type, quality).split(',')[1];
            setTimeout(function () {
                var binStr = atob(dataURL),
                    len = binStr.length,
                    arr = new Uint8Array(len);
                for (var i = 0; i < len; i++) {
                    arr[i] = binStr.charCodeAt(i);
                }
                callback(new Blob([arr], { type: type || 'image/png' }));
            });
        }
    });
}

var header_title = 'App權限管理平台';
var func_route_map = [
    { name: "應用程式管理", route: "/", order: 0, icon: "home" },
    { name: "使用者畫面預覽", route: "/demo", order: 1, icon: "pageview" },
];

var dragOptions = function () {
    return {
        animation: 200,
        group: "description",
        disabled: false,
        ghostClass: "ghost"
    };
};
var drag = false;
userInfo = {};



// Component

var Demo = {
    template: '#demo_area',
    data: function () {
        return {
            account: '',
            name: '',
            menu: {},
            app_list: [],
            appID: 1,
            menuID: 1,
            func_uuid: '',
            isLoading: false,
        }
    },
    mounted: function () {
        var self = this;

        axios.post('api/AppBase', {
            accessToken: self.$cookies.get('userInfo').accessToken,
        })
            .then(function (response) {
                self.app_list = response.data;

                self.$nextTick(function () {
                    $('select').formSelect();
                });
            })
            .catch(function (error) { console.log(error) });

        //axios.post('api/MainMenu/getMainMenuByAccount', {
        //    accessToken: self.$cookies.get('userInfo').accessToken,
        //    appID: 2,
        //    keywords: self.account
        //})
        //    .then(function (response) {
        //        self.menu = response.data.menu;
        //    })
        //    .catch(function (error) { console.log(error) });

        //axios.post('api/UserPermission/getDemoUserPermissions', {
        //    accessToken: self.$cookies.get('userInfo').accessToken,
        //    appID: 2,
        //    keywords: "N000171394"
        //})
        //    .then(function (response) {
        //        self.app = response.data;
        //    })
        //    .catch(function (error) { console.log(error) });
    },
    methods: {
        queryMainMenu: function () {
            var self = this;

            axios.post('api/MainMenu/getMainMenuByAccount', {
                accessToken: self.$cookies.get('userInfo').accessToken,
                appID: self.appID,
                keywords: self.account,
                uuid: self.func_uuid,
                menuID: self.menuID
            })
                .then(function (response) {
                    if (!response.data || !response.data.menu.length) {
                        M.toast({ html: "無資料 !" });
                        if (self.menuID === 2) {
                            self.menuID = 1;
                        } else {
                            self.menu = {};
                        }
                    } else {
                        self.menu = response.data.menu;
                    }
                    self.isLoading = false;
                    self.func_uuid = '';
                })
                .catch(function (error) { console.log(error) });
        },
        func_click: function (func) {
            if (func.hasSubFunction) {
                var self = this;
                self.isLoading = true;

                self.menuID = 2;
                self.func_uuid = func.uuid;
                self.queryMainMenu();
            }
        },
        return_click: function () {
            var self = this;
            if (!self.account) {
                M.toast({ html: "請輸入並選擇使用者資訊 !" });
                return;
            }

            self.isLoading = true;

            self.menuID = 1;
            self.queryMainMenu();
        },
        select_auto: function (txt) {
            var self = this;
            self.isLoading = true;

            this.$emit('change_auto_trigger', true);

            var values = txt.split(' ');

            // 強制 Autocomplete 只秀編號
            self.account = '';
            self.account = values[0];

            self.name = values[1];
            self.menuID = 1;
            self.queryMainMenu();
        },
        sleep: function (ms) {
            return new Promise(function (resolve) { setTimeout(resolve, ms) });
        },
        // Babel Transfer
        auto_select: function () {
            var _auto_select = _asyncToGenerator(
                /*#__PURE__*/
                regeneratorRuntime.mark(function _callee() {
                    var self, delay, now, gap;
                    return regeneratorRuntime.wrap(function _callee$(_context) {
                        while (1) {
                            switch (_context.prev = _context.next) {
                                case 0:
                                    self = this; // 延遲

                                    delay = 1500;
                                    self.input_last = new Date().valueOf();
                                    _context.next = 5;
                                    return self.sleep(delay);

                                case 5:
                                    now = new Date().valueOf();
                                    gap = now - self.input_last;

                                    if (!(gap < delay)) {
                                        _context.next = 9;
                                        break;
                                    }

                                    return _context.abrupt("return");

                                case 9:
                                    // 搜尋使用者
                                    axios.post('api/Account/getAutoAccounts', {
                                        accessToken: self.$cookies.get('userInfo').accessToken,
                                        keywords: self.account
                                    }).then(function (response) {
                                        var result = response.data;
                                        var data = {};

                                        if (!!result.length) {
                                            for (var i = 0; i < result.length; i++) {
                                                data[result[i]["Account"] + ' ' + result[i]["name"]] = null;
                                            }

                                            $('input.autocomplete').autocomplete({
                                                data: data,
                                                limit: 5,
                                                onAutocomplete: function onAutocomplete(txt) {
                                                    self.select_auto(txt);
                                                }
                                            });
                                            $('input.autocomplete').autocomplete('open');
                                        }
                                    }).catch(function (error) {
                                        console.log(error);
                                    });

                                case 10:
                                case "end":
                                    return _context.stop();
                            }
                        }
                    }, _callee, this);
                }));

            function auto_select() {
                return _auto_select.apply(this, arguments);
            }

            return auto_select;
        }()
    }
};

var BreadCrumb = {
    template: '#breadcrumb',
    data: function () {
        return {
            bc_list: [
                {
                    name: '應用程式',
                    route: '/'
                }
            ],
        }
    },
    mounted: function () {
        var self = this;

        if (self.$cookies.get('userInfo') !== null) {
            // App 名稱
            axios.post('api/AppBase', {
                accessToken: self.$cookies.get('userInfo').accessToken,
                appID: self.$route.params.appID,
            })
                .then(function (response) {
                    self.bc_list.push({
                        name: response.data[0].name,
                        route: '/function_group/' + self.$route.params.appID
                    });
                })
                .then(function () {
                    // Group 名稱
                    axios.post('api/FunctionGroupBase/getFunctionGroupBases', {
                        accessToken: self.$cookies.get('userInfo').accessToken,
                        appID: self.$route.params.appID,
                    })
                        .then(function (response) {
                            var groups = response.data.data.filter(function (f) { return f.functionGroupID.toString() === self.$route.params.functionGroupID })
                            if (groups.length > 0) {
                                self.bc_list.push({
                                    name: groups[0].name,
                                    route: '/app_function/' + self.$route.params.appID + '/' + self.$route.params.functionGroupID
                                });
                            }
                        }).then(function () {
                            // Function 名稱
                            axios.post('api/AppFunction/getAppFunctions', {
                                accessToken: self.$cookies.get('userInfo').accessToken,
                                appID: self.$route.params.appID,
                                functionGroupID: self.$route.params.functionGroupID,
                            })
                                .then(function (response) {
                                    var funcs = response.data.data.filter(function (f) { return f.functionID.toString() === self.$route.params.functionID })
                                    if (funcs.length > 0) {
                                        self.bc_list.push({
                                            name: funcs[0].name,
                                            route: '/sub_app_function/' + self.$route.params.appID + '/' + self.$route.params.functionGroupID + '/' + self.$route.params.functionID
                                        });
                                    }
                                })
                                .then(function () {
                                    // Sub Function 名稱
                                    axios.post('api/SubAppFunction/getSubAppFunctions', {
                                        accessToken: self.$cookies.get('userInfo').accessToken,
                                        appID: self.$route.params.appID,
                                        functionGroupID: self.$route.params.functionGroupID,
                                        functionID: self.$route.params.functionID,
                                    })
                                        .then(function (response) {
                                            var subfuncs = response.data.data.filter(function (f) { return f.functionID.toString() === self.$route.params.subFunctionID })
                                            if (subfuncs.length > 0) {
                                                self.bc_list.push({
                                                    name: subfuncs[0].name,
                                                    route: self.$router.currentRoute
                                                });
                                            }
                                        })
                                        .catch(function (error) { console.log(error) });
                                })
                                .catch(function (error) { console.log(error) });
                        })
                        .catch(function (error) { console.log(error) });
                })
                .catch(function (error) { console.log(error) });
        }
    }
}

var DelConfirm = {
    template: '#del_confirm',
    props: ['item'],
    computed: {
        get_title: function () {
            var self = this;
            return self.item.name;
        }
    },
    methods: {
        del: function () {
            this.$emit('del');
        },
    }
}

var FuncGroupItem = {
    template: '#func_group_item',
    props: ['item', 'cache_data', 'isChange', 'isMovable'],
    computed: {
        validation: function () {
            var self = this;
            var nameCount = 0;
            //var functionGroupCodeCount = 0;

            if (!!self.item.name) {
                nameCount = self.item.name.length;
            }

            return (nameCount === 0);

            //if (!!self.item.functionGroupCode) {
            //    functionGroupCodeCount = self.item.functionGroupCode.length;
            //}
            
            //return (nameCount === 0) || (functionGroupCodeCount === 0);
        }
    },
    methods: {
        add: function () {
            this.$emit('add');
        },
        upd: function () {
            this.$emit('upd');
        },
    }
}

var AppFuncItem = {
    template: '#app_function_item',
    props: ['item', 'cache_data', 'isChange', 'isMovable'],
    computed: {
        validation: function () {
            var self = this;
            var nameCount = 0;
            //var functionCodeCount = 0;

            if (!!self.item.name) {
                nameCount = self.item.name.length;
            }

            return (nameCount === 0);

            //if (!!self.item.functionCode) {
            //    functionCodeCount = self.item.functionCode.length;
            //}

            //return (nameCount === 0) || (functionCodeCount === 0);
        }
    },
    methods: {
        add: function () {
            this.$emit('add');
        },
        upd: function () {
            this.$emit('upd');
        },
    }
}

var FloatAction = {
    template: '#float-action',
    mounted: function () {
        $('.fixed-action-btn').floatingActionButton({
            toolbarEnabled: true,
        });
    },
    methods: {
        back: function () {
            this.$router.back();
        },
        home: function () {
            this.$router.push('/');
        },
        demo: function () {
            this.$router.push('/demo');
        },
    }
};

var Header = {
    template: '#header_area',
    created: function () {
        if (!this.$cookies.get('userInfo')) {
            this.$router.push('/login');
        }
    },
    data: function () {
        return {
            header_title: header_title,
            func_route_map: func_route_map,
            isTest: true,
        }
    },
    mounted: function(){
        $('.sidenav').sidenav();

        var self = this;

        if (self.$cookies.get('userInfo') !== null) {
            axios.post('api/Auth/isTest',
                { accessToken: self.$cookies.get('userInfo').accessToken, })
                .then(function (response) {
                    self.isTest = response.data;
                    if (self.isTest) {
                        self.header_title = self.header_title + " (測試環境)";
                    }
                })
                .catch(function (error) { console.log(error) });
        }
    },
    methods: {
        logout: function () {
            this.$cookies.remove('userInfo');
            this.$router.push('/login');
        }
    }
};

var Login = {
    template: '#login_area',
    created: function () {
        if (this.$cookies.get('userInfo') != null) {
            this.$router.push('/');
        }
    },
    data: function () {
        return {
            account: "",
            password: "",
            userInfo: this.$cookies.get('userInfo')
        }
    },
    methods: {
        login: function () {
            $('.preloader-background').fadeIn('slow');
            var self = this;
            axios.post('api/Auth/Login', { account: self.account, password: self.password })
                .then(function (response) {
                    self.userInfo = response.data;
                    if (self.userInfo.result === "SUCCESS") {
                        self.$cookies.set('userInfo', self.userInfo)
                        self.$router.push('/');
                    } else {
                        M.toast({ html: "帳號密碼輸入錯誤或您沒有使用權限 !" });
                    }
                    $('.preloader-background').fadeOut('slow');
                })
                .catch(function (error) { console.log(error) });
        }
    }
};

var AppManage = {
    template: '#app_manage',
    data: function () {
        return {
            func_name: '應用程式清單',
            app_list: [],
        }
    },
    mounted: function () {
        var self = this;

        if (self.$cookies.get('userInfo') !== null) {
            axios.post('api/AppBase', {
                accessToken: self.$cookies.get('userInfo').accessToken,
            })
                .then(function (response) {
                    self.app_list = response.data;
                })
                .catch(function (error) { console.log(error) });
        }
    },
    computed: {
        dragOptions: dragOptions
    },
    methods: {
        go_group_list: function (element) {
            this.$router.push('/function_group/' + element.appID);
        },
    }
};

var FunctionGroup = {
    template: '#function_group',
    data: function () {
        return {
            func_name: '群組',
            func_group_list: [],
            isMovable: false,
            drag: drag,
            item: {
                name: '',
                functionGroupCode: ''
            },
            search: '',
            cache_data: {},
            isChange: false,
        }
    },
    components: {
        'func_group_item': FuncGroupItem,
        'del_confirm': DelConfirm,
        'bread_crumb': BreadCrumb,
    },
    mounted: function () {
        var self = this;

        if (self.$cookies.get('userInfo') !== null) {
            // Group 清單
            axios.post('api/FunctionGroupBase/getFunctionGroupBases', {
                accessToken: self.$cookies.get('userInfo').accessToken,
                appID: self.$route.params.appID,
            })
                .then(function (response) {
                    self.func_group_list = response.data.data;
                    self.isMovable = response.data.isMovable;

                    self.$nextTick(function () {
                        $('.modal').modal({
                            dismissible: true,
                            onOpenEnd: function () {
                                self.cache_data = Object.assign({}, self.item);
                            },
                            onCloseStart: function () {
                                $('.modal').scrollTop(0);
                            },
                            onCloseEnd: function (ele) {
                                if (!self.isChange) {
                                    for (var i = 0; i < self.func_group_list.length; i++) {
                                        if (self.func_group_list[i].uuid === self.cache_data.uuid) {
                                            self.func_group_list[i].name = self.cache_data.name;
                                            self.func_group_list[i].functionGroupCode = self.cache_data.functionGroupCode;
                                            self.func_group_list[i].isEnabled = self.cache_data.isEnabled;
                                        }
                                    }
                                }
                                self.isChange = false;
                            }
                        });
                    })
                })
                .catch(function (error) { console.log(error) });
        }
    },
    computed: {
        dragOptions: dragOptions,
        search_list: function () {
            var self = this;

            if (self.func_group_list.length > 0) {
                return self.func_group_list.filter(
                    function (f) {
                        return f.name.includes(self.search) || f.functionGroupID.toString().includes(self.search);
                    });
            }
            return self.func_group_list;
        }
    },
    methods: {
        add: function () {
            var self = this;

            self.item.appID = self.$route.params.appID;
            axios.post('api/FunctionGroupBase/addFunctionGroupBase', {
                accessToken: self.$cookies.get('userInfo').accessToken,
                appID: self.item.appID,
                sortList: self.item
            })
                .then(function (response) {
                    self.func_group_list.push(response.data);
                    M.toast({ html: "新增成功" });
                })
                .catch(function (error) { console.log(error) });
        },
        upd: function () {
            var self = this;

            axios.post('api/FunctionGroupBase/updateFunctionGroupBases', {
                accessToken: this.$cookies.get('userInfo').accessToken,
                sortList: [self.item]
            })
                .then(function (response) {
                    //self.func_group_list = response.data;
                    M.toast({ html: "儲存完成" });
                    self.isChange = true;
                })
                .catch(function (error) { console.log(error) });
        },
        del: function () {
            var self = this;
            axios.post('api/FunctionGroupBase/deleteFunctionGroupBase', {
                accessToken: this.$cookies.get('userInfo').accessToken,
                uuid: self.item.uuid
            })
                .then(function (response) {
                    self.func_group_list = self.func_group_list.filter(function (value, index, arr) { return value.uuid !== response.data });
                    M.toast({ html: "刪除完成" });
                    self.isChange = true;
                })
                .catch(function (error) { console.log(error) });
        },
        save_sort: function () {
            var self = this;

            axios.post('api/FunctionGroupBase/updateFunctionGroupBases', {
                accessToken: this.$cookies.get('userInfo').accessToken,
                sortList: self.func_group_list
            })
                .then(function (response) {
                    self.func_group_list = response.data;
                    M.toast({ html: "儲存完成" });
                })
                .catch(function (error) { console.log(error) });
            
        },
        sort: function () {
            this.func_group_list = this.func_group_list.sort(
                function (a, b) {
                    return a.sortOrder - b.sortOrder;
                }
            );
        },
        choice_item: function (item) {
            this.item = item;
        },
        go_function_list: function (element) {
            this.$router.push('/app_function/' + element.appID + '/' + element.functionGroupID);
        },
        go_permission: function (type, element) {
            this.$router.push('/priv/' + type + '/' + element.appID + '/' + element.functionGroupID);
        }
    }
};

var AppFunction = {
    template: '#app_function',
    data: function () {
        return {
            func_name: '功能區',
            app_function_list: [],
            isMovable: false,
            drag: drag,
            item: {
                name: '',
                functionGroupCode: ''
            },
            search: '',
            cache_data: {},
            isChange: false,
        }
    },
    components: {
        'app_function_item': AppFuncItem,
        'del_confirm': DelConfirm,
        'bread_crumb': BreadCrumb,
    },
    mounted: function () {
        var self = this;
        $('.dropify').dropify();
        if (self.$cookies.get('userInfo') !== null) {
            // Function 清單
            axios.post('api/AppFunction/getAppFunctions', {
                accessToken: this.$cookies.get('userInfo').accessToken,
                appID: this.$route.params.appID,
                functionGroupID: this.$route.params.functionGroupID,
            })
                .then(function (response) {
                    self.app_function_list = response.data.data;
                    self.isMovable = response.data.isMovable;

                    self.$nextTick(function () {
                        $('.modal').modal({
                            dismissible: true,
                            onOpenEnd: function () {
                                self.cache_data = Object.assign({}, self.item);

                                var dropify = $('.dropify').dropify();
                                dropify = dropify.data('dropify');
                                dropify.resetPreview();
                                dropify.clearElement();
                                dropify.settings.defaultFile = self.item.iconURL;
                                dropify.destroy();
                                dropify.init();
                            },
                            onCloseStart: function () {
                                $('.modal').scrollTop(0);
                            },
                            onCloseEnd: function (ele) {
                                if (!self.isChange) {
                                    for (var i = 0; i < self.app_function_list.length; i++) {
                                        if (self.app_function_list[i].uuid === self.cache_data.uuid) {
                                            self.app_function_list[i].functionCode = self.cache_data.functionCode;
                                            self.app_function_list[i].name = self.cache_data.name;
                                            self.app_function_list[i].comment = self.cache_data.comment;
                                            self.app_function_list[i].isEnabled = self.cache_data.isEnabled;
                                            self.app_function_list[i].hasSubFunction = self.cache_data.hasSubFunction;
                                            self.app_function_list[i].defaultPermission = self.cache_data.defaultPermission;
                                            self.app_function_list[i].iconURL = self.cache_data.iconURL;
                                            self.app_function_list[i].webURL = self.cache_data.webURL;
                                        }
                                    }
                                }
                                self.isChange = false;
                            }
                        });

                        $('.materialboxed').materialbox();
                    })
                })
                .catch(function (error) { console.log(error) });
        }
    },
    computed: {
        dragOptions: dragOptions,
        search_list: function () {
            var self = this;

            if (self.app_function_list.length > 0) {
                return self.app_function_list.filter(
                    function (f) {
                        return f.name.includes(self.search) || f.functionID.toString().includes(self.search) || f.comment.includes(self.search);
                    });
            }
            return self.app_function_list;
        }
    },
    methods: {
        add: function () {
            var self = this;
            self.item.appID = this.$route.params.appID;
            self.item.functionGroupID = this.$route.params.functionGroupID;

            if ($('.dropify').get(0).files.length > 0) {
                var width = 200;
                var height = 200;
                var fileName = $('.dropify').get(0).files[0].name;
                var reader = new FileReader();
                reader.readAsDataURL($('.dropify').get(0).files[0]);
                reader.onload = function (event) {
                    var img = new Image();
                    img.src = event.target.result;
                    img.onload = function () {
                        var elem = document.createElement('canvas');
                        elem.width = width;
                        elem.height = height;
                        var ctx = elem.getContext('2d');
                        // img.width and img.height will contain the original dimensions
                        ctx.drawImage(img, 0, 0, width, height);
                        ctx.canvas.toBlob(
                            function (blob) {
                                var fd = new FormData();
                                fd.append('file', blob, fileName);
                                fd.append('accessToken', self.$cookies.get('userInfo').accessToken);
                                fd.append('sortList', JSON.stringify(self.item));
                                axios.post('api/AppFunction/addAppFunction', fd)
                                    .then(function (response) {
                                        response.data.files = null;

                                        self.app_function_list.push(response.data);
                                        M.toast({ html: "新增成功" });
                                    })
                                    .catch(function (error) { console.log(error) });
                            }
                            , $('.dropify').get(0).files[0].type, 1);
                    },
                        reader.onerror = function (error) { console.log(error) };
                };
            } else {
                var fd = new FormData();
                fd.append('accessToken', self.$cookies.get('userInfo').accessToken);
                fd.append('sortList', JSON.stringify(self.item));
                axios.post('api/AppFunction/addAppFunction', fd)
                    .then(function (response) {
                        response.data.files = null;

                        self.app_function_list.push(response.data);
                        M.toast({ html: "新增成功" });
                    })
                    .catch(function (error) { console.log(error) });
            }
        },
        upd: function () {
            var self = this;

            if ($('.dropify').get(0).files.length > 0) {
                var width = 200;
                var height = 200;
                var fileName = $('.dropify').get(0).files[0].name;
                var reader = new FileReader();
                reader.readAsDataURL($('.dropify').get(0).files[0]);
                reader.onload = function (event) {
                    var img = new Image();
                    img.src = event.target.result;
                    img.onload = function () {
                        var elem = document.createElement('canvas');
                        elem.width = width;
                        elem.height = height;
                        var ctx = elem.getContext('2d');
                        // img.width and img.height will contain the original dimensions
                        ctx.drawImage(img, 0, 0, width, height);
                        ctx.canvas.toBlob(
                            function (blob) {
                                var fd = new FormData();
                                fd.append('accessToken', self.$cookies.get('userInfo').accessToken);
                                fd.append('sortList', '[' + JSON.stringify(self.item) + ']');
                                fd.append('file', blob, fileName);

                                axios.post('api/AppFunction/updateAppFunctions', fd)
                                    .then(function (response) {
                                        M.toast({ html: "儲存完成" });
                                        self.isChange = true;
                                        self.item = response.data[0];

                                        for (var i = 0; i < self.app_function_list.length; i++) {
                                            if (self.app_function_list[i].uuid === response.data[0].uuid) {
                                                self.app_function_list[i].iconURL = response.data[0].iconURL;
                                            }
                                        }
                                    })
                                    .catch(function (error) { console.log(error) });
                            }
                            , $('.dropify').get(0).files[0].type, 1);
                    },
                        reader.onerror = function (error) { console.log(error) };
                };
            } else {
                var fd = new FormData();
                fd.append('accessToken', self.$cookies.get('userInfo').accessToken);
                fd.append('sortList', '[' + JSON.stringify(self.item) + ']');

                axios.post('api/AppFunction/updateAppFunctions', fd)
                    .then(function (response) {
                        M.toast({ html: "儲存完成" });
                        self.isChange = true;
                        self.item = response.data[0];

                        for (var i = 0; i < self.app_function_list.length; i++) {
                            if (self.app_function_list[i].uuid === response.data[0].uuid) {
                                self.app_function_list[i].iconURL = response.data[0].iconURL;
                            }
                        }
                    })
                    .catch(function (error) { console.log(error) });
            }
        },
        del: function () {
            var self = this;
            axios.post('api/AppFunction/deleteAppFunction', {
                accessToken: this.$cookies.get('userInfo').accessToken,
                uuid: self.item.uuid
            })
                .then(function (response) {
                    self.app_function_list = self.app_function_list.filter(function (value, index, arr) { return value.uuid !== response.data });
                    M.toast({ html: "刪除完成" });
                    self.isChange = true;
                })
                .catch(function (error) { console.log(error) });
        },
        save_sort: function () {
            var self = this;

            var fd = new FormData();
            fd.append('accessToken', self.$cookies.get('userInfo').accessToken);
            fd.append('sortList', JSON.stringify(self.app_function_list));

            axios.post('api/AppFunction/updateAppFunctions', fd)
                .then(function (response) {
                    self.app_function_list = response.data;
                    M.toast({ html: "儲存完成" });
                })
                .catch(function (error) { console.log(error) });

        },
        sort: function () {
            this.app_function_list = this.app_function_list.sort(
                function (a, b) {
                    return a.sortOrder - b.sortOrder;
                }
            );
        },
        choice_item: function (item) {
            this.item = item;
        },
        go_permission: function (type, element) {
            this.$router.push('/priv/' + type + '/' + element.appID + '/' + element.functionGroupID + '/' + element.functionID);
        },
        go_sub_function_list: function (element) {
            this.$router.push('/sub_app_function/' + element.appID + '/' + element.functionGroupID + '/' + element.functionID);
        },
    }
};

var SubAppFunction = {
    template: '#app_function',
    data: function () {
        return {
            func_name: '子功能區',
            app_function_list: [],
            isMovable: false,
            drag: drag,
            item: {
                name: '',
                functionGroupCode: ''
            },
            search: '',
            cache_data: {},
            isChange: false,
        }
    },
    components: {
        'app_function_item': AppFuncItem,
        'del_confirm': DelConfirm,
        'bread_crumb': BreadCrumb,
    },
    mounted: function () {
        var self = this;
        $('.dropify').dropify();
        if (self.$cookies.get('userInfo') !== null) {
            // Sub Function 清單
            axios.post('api/SubAppFunction/getSubAppFunctions', {
                accessToken: self.$cookies.get('userInfo').accessToken,
                appID: self.$route.params.appID,
                functionGroupID: self.$route.params.functionGroupID,
                functionID: self.$route.params.functionID,
            })
                .then(function (response) {
                    self.app_function_list = response.data.data;
                    self.isMovable = response.data.isMovable;

                    self.$nextTick(function () {
                        $('.modal').modal({
                            dismissible: true,
                            onOpenEnd: function () {
                                self.cache_data = Object.assign({}, self.item);

                                var dropify = $('.dropify').dropify();
                                dropify = dropify.data('dropify');
                                dropify.resetPreview();
                                dropify.clearElement();
                                dropify.settings.defaultFile = self.item.iconURL;
                                dropify.destroy();
                                dropify.init();
                            },
                            onCloseStart: function () {
                                $('.modal').scrollTop(0);
                            },
                            onCloseEnd: function (ele) {
                                if (!self.isChange) {
                                    for (var i = 0; i < self.app_function_list.length; i++) {
                                        if (self.app_function_list[i].uuid === self.cache_data.uuid) {
                                            self.app_function_list[i].functionCode = self.cache_data.functionCode;
                                            self.app_function_list[i].name = self.cache_data.name;
                                            self.app_function_list[i].comment = self.cache_data.comment;
                                            self.app_function_list[i].isEnabled = self.cache_data.isEnabled;
                                            self.app_function_list[i].hasSubFunction = self.cache_data.hasSubFunction;
                                            self.app_function_list[i].defaultPermission = self.cache_data.defaultPermission;
                                            self.app_function_list[i].iconURL = self.cache_data.iconURL;
                                            self.app_function_list[i].webURL = self.cache_data.webURL;
                                        }
                                    }
                                }
                                self.isChange = false;
                            }
                        });

                        $('.materialboxed').materialbox();
                    })
                })
                .catch(function (error) { console.log(error) });
        }
    },
    computed: {
        dragOptions: dragOptions,
        search_list: function () {
            var self = this;

            if (self.app_function_list.length > 0) {
                return self.app_function_list.filter(
                    function (f) {
                        return f.name.includes(self.search) || f.functionID.toString().includes(self.search) || f.comment.includes(self.search);
                    });
            }
            return self.app_function_list;
        }
    },
    methods: {
        add: function () {
            var self = this;
            self.item.appID = self.$route.params.appID;
            self.item.functionGroupID = this.$route.params.functionGroupID;
            self.item.appFunctionID = this.$route.params.functionID;

            if ($('.dropify').get(0).files.length > 0) {
                var width = 200;
                var height = 200;
                var fileName = $('.dropify').get(0).files[0].name;
                var reader = new FileReader();
                reader.readAsDataURL($('.dropify').get(0).files[0]);
                reader.onload = function (event) {
                    var img = new Image();
                    img.src = event.target.result;
                    img.onload = function () {
                        var elem = document.createElement('canvas');
                        elem.width = width;
                        elem.height = height;
                        var ctx = elem.getContext('2d');
                        // img.width and img.height will contain the original dimensions
                        ctx.drawImage(img, 0, 0, width, height);
                        ctx.canvas.toBlob(
                            function (blob) {
                                var fd = new FormData();
                                fd.append('accessToken', self.$cookies.get('userInfo').accessToken);
                                fd.append('sortList', JSON.stringify(self.item));
                                fd.append('file', blob, fileName);
                                console.log(fd);
                                axios.post('api/SubAppFunction/addSubAppFunction', fd)
                                    .then(function (response) {
                                        response.data.files = null;

                                        self.app_function_list.push(response.data);
                                        M.toast({ html: "新增成功" });
                                    })
                                    .catch(function (error) { console.log(error) });
                            }
                            , $('.dropify').get(0).files[0].type, 1);
                    },
                        reader.onerror = function (error) { console.log(error) };
                };
            } else {
                var fd = new FormData();
                fd.append('accessToken', self.$cookies.get('userInfo').accessToken);
                fd.append('sortList', JSON.stringify(self.item));
                axios.post('api/SubAppFunction/addSubAppFunction', fd)
                    .then(function (response) {
                        response.data.files = null;

                        self.app_function_list.push(response.data);
                        M.toast({ html: "新增成功" });
                    })
                    .catch(function (error) { console.log(error) });
            }
        },
        upd: function () {
            var self = this;

            if ($('.dropify').get(0).files.length > 0) {
                var width = 200;
                var height = 200;
                var fileName = $('.dropify').get(0).files[0].name;
                var reader = new FileReader();
                reader.readAsDataURL($('.dropify').get(0).files[0]);
                reader.onload = function (event) {
                    var img = new Image();
                    img.src = event.target.result;
                    img.onload = function () {
                        var elem = document.createElement('canvas');
                        elem.width = width;
                        elem.height = height;
                        var ctx = elem.getContext('2d');
                        // img.width and img.height will contain the original dimensions
                        ctx.drawImage(img, 0, 0, width, height);
                        ctx.canvas.toBlob(
                            function (blob) {
                                var fd = new FormData();
                                fd.append('accessToken', self.$cookies.get('userInfo').accessToken);
                                fd.append('sortList', '[' + JSON.stringify(self.item) + ']');
                                fd.append('file', blob, fileName);

                                axios.post('api/SubAppFunction/updateSubAppFunctions', fd)
                                    .then(function (response) {
                                        M.toast({ html: "儲存完成" });
                                        self.isChange = true;
                                        self.item = response.data[0];

                                        for (var i = 0; i < self.app_function_list.length; i++) {
                                            if (self.app_function_list[i].uuid === response.data[0].uuid) {
                                                self.app_function_list[i].iconURL = response.data[0].iconURL;
                                            }
                                        }
                                    })
                                    .catch(function (error) { console.log(error) });
                            }
                            , $('.dropify').get(0).files[0].type, 1);
                    },
                        reader.onerror = function (error) { console.log(error) };
                };
            } else {
                var fd = new FormData();
                fd.append('accessToken', self.$cookies.get('userInfo').accessToken);
                fd.append('sortList', '[' + JSON.stringify(self.item) + ']');

                axios.post('api/SubAppFunction/updateSubAppFunctions', fd)
                    .then(function (response) {
                        M.toast({ html: "儲存完成" });
                        self.isChange = true;
                        self.item = response.data[0];

                        for (var i = 0; i < self.app_function_list.length; i++) {
                            if (self.app_function_list[i].uuid === response.data[0].uuid) {
                                self.app_function_list[i].iconURL = response.data[0].iconURL;
                            }
                        }
                    })
                    .catch(function (error) { console.log(error) });
            }
        },
        del: function () {
            var self = this;
            axios.post('api/SubAppFunction/deleteSubAppFunction', {
                accessToken: this.$cookies.get('userInfo').accessToken,
                uuid: self.item.uuid
            })
                .then(function (response) {
                    self.app_function_list = self.app_function_list.filter(function (value, index, arr) { return value.uuid !== response.data });
                    M.toast({ html: "刪除完成" });
                    self.isChange = true;
                })
                .catch(function (error) { console.log(error) });
        },
        save_sort: function () {
            var self = this;

            var fd = new FormData();
            fd.append('accessToken', self.$cookies.get('userInfo').accessToken);
            fd.append('sortList', JSON.stringify(self.app_function_list));

            axios.post('api/SubAppFunction/updateSubAppFunctions', fd)
                .then(function (response) {
                    self.app_function_list = response.data;
                    M.toast({ html: "儲存完成" });
                })
                .catch(function (error) { console.log(error) });

        },
        sort: function () {
            this.app_function_list = this.app_function_list.sort(
                function (a, b) {
                    return a.sortOrder - b.sortOrder;
                }
            );
        },
        choice_item: function (item) {
            this.item = item;
        },
        go_permission: function (type, element) {
            this.$router.push('/priv/' + type + '/' + element.appID + '/' + element.functionGroupID + '/' + element.appFunctionID + '/' + element.functionID);
        }
    }
};

var Privilege = {
    template: '#privielege',
    data: function () {
        return {
            func_name: '權限管理',
            priv_list: [],
            group_priv_list: [],
            group_list: [],
            search: '',
            item: {
                user: '',
                fullUnitID: '',
                isEnabled: true,
                companyID: '',
                groupID: '',
            },
            cache_data: {},
            isChange: false,
            fullunit_list: [],
            isUser: true,
            isAutoTrigger: false,
        }
    },
    components: {
        'del_confirm': {
            template: '#del_confirm',
            props: ['item', 'isUser'],
            computed: {
                get_title: function () {
                    var self = this;
                    if (self.item.groupID && self.item.groupID.length > 0) {
                        return self.item.name;
                    } else {
                        return (self.isUser ? self.item.user : self.item.fullUnitID) + " " + (self.item.name === null ? '' : self.item.name) + (self.isUser ? '' : (self.item.regionID !== '-1' ? '(' + self.item.regionID + ')' : ''));
                    }
                }
            },
            methods: {
                del: function () {
                    this.$emit('del');
                },
            }
        },
        'privilege_item': {
            template: '#privilege_item',
            props: ['item', 'cache_data', 'isChange', 'fullunit_list', 'group_list', 'isUser', 'isAutoTrigger'],
            data: function(){
                return {
                    input_last: 0,
                    autoList: [],
                    view_group_list: [],
                };
            },
            mounted: function () {
                var self = this;
                Vue.nextTick(function () {
                    var array_of_dom_elements = document.querySelectorAll("input[type=range]");
                    M.Range.init(array_of_dom_elements);
                });

                // 個人群組清單
                axios.post('api/MlFpcRead/getViewGroup', {
                    accessToken: self.$cookies.get('userInfo').accessToken,
                }).then(function (response) {
                    self.view_group_list = response.data;
                
                    self.$nextTick(function () {
                        var elems = document.querySelectorAll('select');
                        M.FormSelect.init(elems, {});
                    })
                }).catch(function (error) {
                    console.log(error);
                });
            },
            computed: {
                validation: function () {
                    var self = this;
                    var name = (self.$route.params.type === 'dept') ? (self.item.groupID && self.item.groupID.length > 0 ? self.item.groupID.length : self.item.companyID + self.item.departmentID) : self.item.user;
                    //var inUserList = Object.values(self.autoList).map(function (item) { return item.Account }).includes(self.item.user);
                    return (name.length === 0 || /[\u4e00-\u9fa5]+/.test(name));
                },
            },
            methods: {
                add: function () {
                    this.$emit('add');
                },
                upd: function () {
                    this.$emit('upd');
                },
                substr_dept: function (item, len) {
                    if (item.fullUnitID === undefined) {
                        return '';
                    }
                    return item.fullUnitID.substr(item.companyID.length, item.fullUnitID.length - len - item.companyID.length);
                },
                select_auto: function (txt) {
                    var self = this;

                    this.$emit('change_auto_trigger', true);

                    var values = txt.split(' ');

                    self.item.name = values[1];
                    if (self.isUser) {
                        // 強制 Autocomplete 只秀編號
                        self.item.user = '';
                        self.item.user = values[0];
                    } else {
                        // 強制 Autocomplete 只秀編號
                        self.item.fullUnitID = '';
                        self.item.fullUnitID = values[0];

                        var dept = self.item.name.split('(').slice(-1)[0].split(')')[0];
                        self.item.companyID = self.item.fullUnitID.replace(dept, '');
                        self.item.departmentID = values[0].substr(self.item.companyID.length);

                        self.changeDept();
                    }
                },
                sleep: function (ms) {
                    return new Promise(function (resolve) { setTimeout(resolve, ms) });
                },
                changeGroup: function () {
                    var self = this;
                    //if (!self.item.uuid) {
                        var group = _.find(self.group_list, function (g) { return g.Guid === self.item.groupID });
                        self.item.comment = group.Remark;
                    //}

                    if (self.item.companyID && self.item.companyID.length !== 0) {
                        self.item.companyID = '';
                        self.item.departmentID = '';
                        self.item.fullUnitID = '';
                    }
                },
                changeDept: function () {
                    var self = this;
                    if (!self.item.uuid) {
                        self.item.comment = '';
                    }

                    if (self.item.groupID && self.item.groupID.length !== 0) {
                        self.item.groupID = '';
                        window.setTimeout(function () {
                            $('select').formSelect();
                        }, 100);
                    }
                },
                // Babel Transfer
                auto_select: function () {
                    var _auto_select = _asyncToGenerator(
                        /*#__PURE__*/
                        regeneratorRuntime.mark(function _callee() {
                            var self, delay, now, gap;
                            return regeneratorRuntime.wrap(function _callee$(_context) {
                                while (1) {
                                    switch (_context.prev = _context.next) {
                                        case 0:
                                            self = this; // 延遲

                                            delay = 1500;
                                            self.input_last = new Date().valueOf();
                                            _context.next = 5;
                                            return self.sleep(delay);

                                        case 5:
                                            now = new Date().valueOf();
                                            gap = now - self.input_last;

                                            if (!(gap < delay)) {
                                                _context.next = 9;
                                                break;
                                            }

                                            return _context.abrupt("return");

                                        case 9:
                                            if (self.isUser) {
                                                // 搜尋使用者
                                                axios.post('api/Account/getAutoAccounts', {
                                                    accessToken: self.$cookies.get('userInfo').accessToken,
                                                    keywords: self.item.user
                                                }).then(function (response) {
                                                    var result = response.data;
                                                    self.autoList = result;
                                                    var data = {};

                                                    for (var i = 0; i < result.length; i++) {
                                                        data[result[i]["Account"] + ' ' + result[i]["name"]] = null;
                                                    }

                                                    $('#autocomplete-input-user').autocomplete({
                                                        data: data,
                                                        limit: 5,
                                                        onAutocomplete: function onAutocomplete(txt) {
                                                            self.select_auto(txt);
                                                        }
                                                    });
                                                    $('#autocomplete-input-user').autocomplete('open');
                                                }).catch(function (error) {
                                                    console.log(error);
                                                });
                                            } else {
                                                // 單位清單
                                                axios.post('api/Account/getFullUnits', {
                                                    accessToken: this.$cookies.get('userInfo').accessToken,
                                                    keywords: self.item.fullUnitID
                                                }).then(function (response) {
                                                    //self.fullunit_list = response.data;
                                                    var result = response.data;
                                                    var data = {};

                                                    for (var i = 0; i < result.length; i++) {
                                                        data[result[i]["fullUnitID"] + ' ' + result[i]["departName"]] = null;
                                                    }

                                                    $('#autocomplete-input-dept').autocomplete({
                                                        data: data,
                                                        limit: 5,
                                                        onAutocomplete: function onAutocomplete(txt) {
                                                            self.select_auto(txt);
                                                        }
                                                    });
                                                    $('#autocomplete-input-dept').autocomplete('open');
                                                }).catch(function (error) {
                                                    console.log(error);
                                                    });
                                            }

                                        case 10:
                                        case "end":
                                            return _context.stop();
                                    }
                                }
                            }, _callee, this);
                        }));

                    function auto_select() {
                        return _auto_select.apply(this, arguments);
                    }

                    return auto_select;
                }()
            }
        },
        'bread_crumb': BreadCrumb,
    },
    mounted: function () {
        var self = this;

        self.isUser = self.$route.params.type === 'user';

        self.func_name = (self.isUser ? '使用者' : '部門／群組') + self.func_name;

        var url = (self.isUser ?
            (self.$route.params.subFunctionID ? 'api/SubUserPermission/getSubUserPermissions' : 'api/UserPermission/getUserPermissions') :
            (self.$route.params.subFunctionID ? 'api/SubDepartmentPermission/getSubDepartmentPermissions' : 'api/DepartmentPermission/getDepartmentPermissions'));

        if (self.$cookies.get('userInfo') !== null) {
            axios.post(url, {
                accessToken: self.$cookies.get('userInfo').accessToken,
                appID: self.$route.params.appID,
                functionGroupID: self.$route.params.functionGroupID,
                functionID: self.$route.params.functionID,
                subFunctionID: self.$route.params.subFunctionID,
            })
                .then(function (response) {
                    self.priv_list = response.data;
                    if (!self.isUser) {
                        for (var i = 0; i < self.priv_list.length; i++) {
                            if (self.priv_list[i].departmentID === '-1') {
                                self.priv_list[i].departmentID = '';
                                self.priv_list[i].fullUnitID = self.priv_list[i].fullUnitID.replace('-1', '');
                            }
                        }
                    }

                    self.$nextTick(function () {
                        $('.modal').modal({
                            dismissible: true,
                            onOpenEnd: function () {
                                self.cache_data = Object.assign({}, self.item);
                            },
                            onCloseStart: function () {
                                $('.modal').scrollTop(0);
                            },
                            onCloseEnd: function (ele) {
                                if (!self.isChange) {
                                    if (self.cache_data.groupID) {
                                        for (var i = 0; i < self.group_priv_list.length; i++) {
                                            if (self.group_priv_list[i].uuid === self.cache_data.uuid) {
                                                self.group_priv_list[i].name = self.cache_data.name;
                                                self.group_priv_list[i].level = self.cache_data.level;
                                                self.group_priv_list[i].isEnabled = self.cache_data.isEnabled;
                                                self.group_priv_list[i].comment = self.cache_data.comment;

                                                self.group_priv_list[i].groupID = self.cache_data.groupID;
                                            }
                                        }
                                    } else {
                                        for (var i = 0; i < self.priv_list.length; i++) {
                                            if (self.priv_list[i].uuid === self.cache_data.uuid) {
                                                self.priv_list[i].name = self.cache_data.name;
                                                self.priv_list[i].level = self.cache_data.level;
                                                self.priv_list[i].isEnabled = self.cache_data.isEnabled;
                                                self.priv_list[i].comment = self.cache_data.comment;

                                                if (self.isUser) {
                                                    self.priv_list[i].user = self.cache_data.user;

                                                } else {
                                                    self.priv_list[i].companyID = self.cache_data.companyID;
                                                    self.priv_list[i].departmentID = self.cache_data.departmentID;
                                                    self.priv_list[i].fullUnitID = self.cache_data.fullUnitID;
                                                }
                                            }
                                        }
                                    }

                                }
                                self.isChange = false;
                                self.isAutoTrigger = false;
                            }
                        });
                    })
                })
                .catch(function (error) { console.log(error) });

            // 群組權限
            if (!self.isUser) {
                // 群組清單
                axios.post('api/MlFpcRead/getGroupCount', {
                    accessToken: self.$cookies.get('userInfo').accessToken,
                }).then(function (response) {
                    self.group_list = response.data;
                    self.$nextTick(function () {
                        var elems = document.querySelectorAll('select');
                        M.FormSelect.init(elems, {});
                    })

                    var group_url = (self.$route.params.subFunctionID ? 'api/SubOnedayGroupPermission/getSubOnedayGroupPermissions' : 'api/OnedayGroupPermission/getOnedayGroupPermissions');
                    axios.post(group_url, {
                        accessToken: self.$cookies.get('userInfo').accessToken,
                        appID: self.$route.params.appID,
                        functionGroupID: self.$route.params.functionGroupID,
                        functionID: self.$route.params.functionID,
                        subFunctionID: self.$route.params.subFunctionID,
                    })
                        .then(function (response) {
                            self.group_priv_list = response.data;
                            for (var i = 0; i < self.group_priv_list.length; i++) {
                                var onedayGroup = _.find(self.group_list, function (gp) { return gp.Guid === self.group_priv_list[i].groupID });
                                self.group_priv_list[i].name = onedayGroup ? onedayGroup.GroupName : '';
                            }
                        })
                        .catch(function (error) { console.log(error) });
                }).catch(function (error) {
                    console.log(error);
                });
            }
        }
    },
    computed: {
        search_list: function () {
            var self = this;
            var list = self.priv_list;
            return list.filter(
                function (f) {
                    return f.comment.includes(self.search) || (!!f.name ? f.name.includes(self.search) : false) || f.level.toString().includes(self.search) ||
                        (self.$route.params.type === 'dept' ? (f.companyID.includes(self.search) || f.departmentID.includes(self.search)) : f.user.includes(self.search));
                });
        },
        group_search_list: function () {
            var self = this;
            var list = self.group_priv_list;
            return list.filter(
                function (f) {
                    return f.comment.includes(self.search) || (!!f.name ? f.name.includes(self.search) : false) || f.level.toString().includes(self.search) ||
                        f.name.includes(self.search);
                });
        },
    },
    methods: {
        add: function () {
            var self = this;

            self.item.appID = self.$route.params.appID;
            self.item.functionGroupID = self.$route.params.functionGroupID;
            self.item.functionID = self.$route.params.functionID;

            if (self.isUser) {
                var url = 'api/UserPermission/insertUserPermission';
                if (self.$route.params.subFunctionID) {
                    self.item.appFunctionID = self.$route.params.functionID;
                    self.item.functionID = self.$route.params.subFunctionID;

                    url = 'api/SubUserPermission/insertSubUserPermission';
                }

                axios.post(url, {
                    accessToken: self.$cookies.get('userInfo').accessToken,
                    sortList: self.item,
                })
                    .then(function (response) {
                        self.priv_list.push(response.data);
                        M.toast({ html: "新增成功" });
                    })
                    .catch(function (error) { console.log(error) });
            } else {
                var url;
                var operate = 'insert';
                var dept_ctrl = 'DepartmentPermission';
                var sub_dept_ctrl = 'SubDepartmentPermission';
                var group_ctrl = 'OnedayGroupPermission'
                var sub_group_ctrl = 'SubOnedayGroupPermission';
                var isSub = !!self.$route.params.subFunctionID;
                var isGroup = (self.item.groupID && self.item.groupID.length > 0);

                if (isGroup) {
                    if (isSub) {
                        self.item.appFunctionID = self.$route.params.functionID;
                        self.item.functionID = self.$route.params.subFunctionID;
                        url = 'api/' + sub_group_ctrl + '/' + operate + sub_group_ctrl;
                    } else {
                        url = 'api/' + group_ctrl + '/' + operate + group_ctrl;
                    }
                } else {
                    var com_depart_len = self.item.companyID.length + self.item.departmentID.length;
                    if (self.item.fullUnitID.length !== com_depart_len) {
                        self.item.fullUnitID = self.item.companyID + self.item.departmentID;
                        self.item.name = '';
                    }

                    if (self.item.departmentID === '') {
                        self.item.departmentID = '-1';
                    }

                    if (isSub) {
                        self.item.appFunctionID = self.$route.params.functionID;
                        self.item.functionID = self.$route.params.subFunctionID;
                        url = 'api/' + sub_dept_ctrl + '/' + operate + sub_dept_ctrl;
                    } else {
                        url = 'api/' + dept_ctrl + '/' + operate + dept_ctrl;
                    }
                }

                axios.post(url, {
                    accessToken: self.$cookies.get('userInfo').accessToken,
                    sortList: self.item,
                })
                    .then(function (response) {
                        if (isGroup) {
                            var data = response.data;
                            var group = _.find(self.group_list, function (g) { return g.Guid === response.data.groupID })
                            data.name = group.GroupName;
                            self.group_priv_list.push(data);
                        } else {
                            self.priv_list.push(response.data);
                        }
                        M.toast({ html: "新增成功" });
                    })
                    .catch(function (error) { console.log(error) });
            }

        },
        upd: function () {
            var self = this;

            if (self.isUser) {
                var url = 'api/UserPermission/updateUserPermissions';
                if (self.$route.params.subFunctionID) {
                    self.item.appFunctionID = self.$route.params.functionID;
                    self.item.functionID = self.$route.params.subFunctionID;

                    url = 'api/SubUserPermission/updateSubUserPermissions';
                }

                axios.post(url, {
                    accessToken: this.$cookies.get('userInfo').accessToken,
                    sortList: [self.item]
                })
                    .then(function (response) {
                        //self.priv_list = response.data;
                        var up = self.priv_list.filter(function (f) { return f.uuid === self.item.uuid })[0];
                        up.companyID = response.data[0].companyID
                        up.departName = response.data[0].departName

                        M.toast({ html: "儲存完成" });
                        self.isChange = true;
                    })
                    .catch(function (error) { console.log(error) });
            } else {
                var url;
                var operate = 'update';
                var dept_ctrl = 'DepartmentPermission';
                var sub_dept_ctrl = 'SubDepartmentPermission';
                var group_ctrl = 'OnedayGroupPermission'
                var sub_group_ctrl = 'SubOnedayGroupPermission';
                var isSub = !!self.$route.params.subFunctionID;
                var isGroup = (self.item.groupID && self.item.groupID.length > 0);

                if (isGroup) {
                    if (isSub) {
                        self.item.appFunctionID = self.$route.params.functionID;
                        self.item.functionID = self.$route.params.subFunctionID;
                        url = 'api/' + sub_group_ctrl + '/' + operate + sub_group_ctrl + 's';
                    } else {
                        url = 'api/' + group_ctrl + '/' + operate + group_ctrl + 's';
                    }
                } else {
                    var com_depart_len = self.item.companyID.length + self.item.departmentID.length;
                    if (self.item.fullUnitID.length !== com_depart_len) {
                        self.item.fullUnitID = self.item.companyID + self.item.departmentID;
                        self.item.name = '';
                    }

                    if (self.item.departmentID === '') {
                        self.item.departmentID = '-1';
                        self.item.fullUnitID = self.item.fullUnitID + '-1';
                    }

                    if (isSub) {
                        self.item.appFunctionID = self.$route.params.functionID;
                        self.item.functionID = self.$route.params.subFunctionID;
                        url = 'api/' + sub_dept_ctrl + '/' + operate + sub_dept_ctrl + 's';
                    } else {
                        url = 'api/' + dept_ctrl + '/' + operate + dept_ctrl + 's';
                    }
                }

                axios.post(url, {
                    accessToken: this.$cookies.get('userInfo').accessToken,
                    sortList: [self.item]
                })
                    .then(function (response) {
                        //self.priv_list = response.data;
                        M.toast({ html: "儲存完成" });
                        self.isChange = true;

                        if (isGroup) {
                            var group = _.find(self.group_list, function (g) { return g.Guid === self.item.groupID })
                            self.item.name = group.GroupName;
                        } else {
                            if (self.item.departmentID === '-1') {
                                self.item.departmentID = '';
                                self.item.fullUnitID = self.item.fullUnitID.replace('-1', '');
                            }
                        }
                    })
                    .catch(function (error) { console.log(error) });
            }
        },
        del: function () {
            var self = this;
            if (self.isUser) {
                var url = 'api/UserPermission/deleteUserPermission';
                if (self.$route.params.subFunctionID) {
                    url = 'api/SubUserPermission/deleteSubUserPermission';
                }

                axios.post(url, {
                    accessToken: this.$cookies.get('userInfo').accessToken,
                    uuid: self.item.uuid
                })
                    .then(function (response) {
                        self.priv_list = self.priv_list.filter(function (value, index, arr) { return value.uuid !== response.data });
                        M.toast({ html: "刪除完成" });
                        self.isChange = true;
                    })
                    .catch(function (error) { console.log(error) });
            } else {
                var url;
                var operate = 'delete';
                var dept_ctrl = 'DepartmentPermission';
                var sub_dept_ctrl = 'SubDepartmentPermission';
                var group_ctrl = 'OnedayGroupPermission'
                var sub_group_ctrl = 'SubOnedayGroupPermission';
                var isSub = !!self.$route.params.subFunctionID;
                var isGroup = (self.item.groupID && self.item.groupID.length > 0);

                if (isGroup) {
                    if (isSub) {
                        self.item.appFunctionID = self.$route.params.functionID;
                        self.item.functionID = self.$route.params.subFunctionID;
                        url = 'api/' + sub_group_ctrl + '/' + operate + sub_group_ctrl;
                    } else {
                        url = 'api/' + group_ctrl + '/' + operate + group_ctrl;
                    }
                } else {
                    var com_depart_len = self.item.companyID.length + self.item.departmentID.length;
                    if (self.item.fullUnitID.length !== com_depart_len) {
                        self.item.fullUnitID = self.item.companyID + self.item.departmentID;
                        self.item.name = '';
                    }

                    if (self.item.departmentID === '') {
                        self.item.departmentID = '-1';
                    }

                    if (isSub) {
                        self.item.appFunctionID = self.$route.params.functionID;
                        self.item.functionID = self.$route.params.subFunctionID;
                        url = 'api/' + sub_dept_ctrl + '/' + operate + sub_dept_ctrl;
                    } else {
                        url = 'api/' + dept_ctrl + '/' + operate + dept_ctrl;
                    }
                }

                axios.post(url, {
                    accessToken: this.$cookies.get('userInfo').accessToken,
                    uuid: self.item.uuid
                })
                    .then(function (response) {
                        if (isGroup) {
                            self.group_priv_list = self.group_priv_list.filter(function (value, index, arr) { return value.uuid !== response.data });
                        } else {
                            self.priv_list = self.priv_list.filter(function (value, index, arr) { return value.uuid !== response.data });
                        }
                        
                        M.toast({ html: "刪除完成" });
                        self.isChange = true;
                    })
                    .catch(function (error) { console.log(error) });
            }
        },
        get_title: function (element) {
            var self = this;
            if (self.$route.params.type === 'dept') {
                if (element.groupID && element.groupID.length > 0) {
                    return element.name;
                } else {
                    var name = ' ' + element.name;
                    return element.fullUnitID + ' ' + (element.name !== null ? name : '') + (element.regionID !== '-1' ? '(' + element.regionID + ')' : '');
                }
                
            } else {
                var name = ' ' + element.name;
                return element.user + ' ' + (element.name !== null ? name : '');
            }
        },
        choice_item: function (item) {
            this.item = item;
        },
        choice_group_item: function (item) {
            this.item = item;
            this.$nextTick(function () {
                $('select').formSelect();
            });
        },
        change_auto_trigger: function (bool) {
            var self = this;
            self.isAutoTrigger = bool
        },
    }
};

var router = new VueRouter({
    //mode: 'history',
    routes: [
        { path: '/', components: { default: AppManage, nav: Header, actb: FloatAction } },
        { path: '/login', components: { default: Login } },
        { name: 'function_group', path: '/function_group/:appID', components: { default: FunctionGroup, nav: Header, actb: FloatAction } },
        { name: 'app_function', path: '/app_function/:appID/:functionGroupID', components: { default: AppFunction, nav: Header, actb: FloatAction } },
        { name: 'sub_app_function', path: '/sub_app_function/:appID/:functionGroupID/:functionID', components: { default: SubAppFunction, nav: Header, actb: FloatAction } },
        { name: 'demo', path: '/demo', components: { default: Demo, nav: Header, actb: FloatAction } },

        { name: 'priv', path: '/priv/:type/:appID/:functionGroupID/:functionID?/:subFunctionID?', components: { default: Privilege, nav: Header, actb: FloatAction} }
    ]
})

var app = new Vue({
    router: router,
    el: "#app",
    data: {
        userInfo: userInfo
    },
});