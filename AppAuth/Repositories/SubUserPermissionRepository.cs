﻿using AppAuth.Models;
using AppAuth.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppAuth.Repositories
{
    public class SubUserPermissionRepository
    {
        private static bool isMssql = false;
        public static List<sub_user_permission> getSubUserPermissions()
        {
            string sql = @"select sup.*, a.name, af.appID, af.functionGroupID, af.functionID appFunctionID, a.companyID, a.departName from sub_user_permission sup
                        left join account a on sup.user = a.account
						left join app_function af on sup.mainUUID = af.uuid";
            return DbService.Query<sub_user_permission>(isMssql, sql).ToList();
        }

        public static sub_user_permission insertSubUserPermission(sub_user_permission sup)
        {
            string sql = @"insert into sub_user_permission(user, mainUUID, functionID, level, isEnabled, comment, uuid)
                           values(@user, @mainUUID, @functionID, @level, @isEnabled, @comment, @uuid)
            ";

            DbService.ExecuteNonQuery(sql, sup);

            return getSubUserPermissions().Where(
                w => w.user.Equals(sup.user) && w.mainUUID.Equals(sup.mainUUID) &&
                w.functionID.Equals(sup.functionID)
            ).FirstOrDefault();
        }

        public static object updateSubUserPermissions(IEnumerable<sub_user_permission> supl)
        {
            string sql = @"update sub_user_permission set
                            user = @user,
                            mainUUID = @mainUUID,
                            functionID = @functionID,
                            level = @level,
                            isEnabled = @isEnabled,
                            comment = @comment
                            where uuid = @uuid
            ";
            
            DbService.ExecuteNonQuery(sql, supl);

            return getSubUserPermissions().Where(w => supl.Select(s => s.uuid).Contains(w.uuid));
        }

        public static void deleteSubUserPermission(Guid uuid)
        {
            string sql = @"delete from sub_user_permission where uuid = @uuid";
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("@uuid", uuid);
            DbService.ExecuteNonQuery(sql, dic);
        }
    }
}