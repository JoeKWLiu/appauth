﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;

namespace AppAuth.Repositories
{
    public class MainMenuRepository
    {
        private static string mobilefpc = "mobilefpc";
        private static string factoryCloud = "factoryCloud";

        private static string main = "mainMenuByAccount";
        private static string sub = "subMenuByAccount";

        private static string url = "https://appcloud.fpcetg.com.tw/{0}/api/common/{1}";

        private static Dictionary<int, string> appType = new Dictionary<int, string>()
        {
            { 1, factoryCloud},
            { 2, mobilefpc},
        };

        private static Dictionary<int, string> menuType = new Dictionary<int, string>()
        {
            { 1, main},
            { 2, sub},
        };

        public static manMenuModels getMenu(int appID, int menuID, string account, Guid uuid)
        {
            using (WebClient wc = new WebClient())
            {
                try
                {
                    wc.Encoding = Encoding.UTF8;
                    wc.Headers.Add(HttpRequestHeader.ContentType, "application/x-www-form-urlencoded");
                    NameValueCollection nc = new NameValueCollection();
                    nc["account"] = account;
                    nc["mainUUID"] = uuid.ToString();

                    var response = wc.UploadValues(String.Format(url, appType[appID], menuType[menuID]) , nc);
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    return (manMenuModels)serializer.Deserialize(Encoding.UTF8.GetString(response), typeof(manMenuModels));
                }
                catch (WebException ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }
    }

    public class functionsModels
    {
        public int functionID { get; set; }
        public string functionCode { get; set; }
        public string functionName { get; set; }
        public int sortOrder { get; set; }
        public int level { get; set; }
        public string iconURL { get; set; }
        public string webURL { get; set; }
        public Guid uuid { get; set; }
        public bool hasSubFunction { get; set; }
    }

    public class menuModels
    {
        public int functionGroupID { get; set; }
        public int sortOrder { get; set; }
        public string functionGroupCode { get; set; }
        public string functionGroupName { get; set; }
        public Guid uuid { get; set; }
        public List<functionsModels> functions { get; set; }
    }

    public class manMenuModels
    {
        public string result { get; set; }
        public List<menuModels> menu { get; set; }
    }
}