﻿using AppAuth.Models.mlfpcread;
using AppAuth.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppAuth.Repositories
{
    public class MlFpcReadRepository
    {
        private static bool isMssql = true;
        public static IEnumerable<Func_OM_HGM_ViewGroup> getViewGroup(string idno)
        {
            string sql = @"SELECT * FROM [JWETG].[dbo].[Func_OM_HGM_ViewGroup](@idno)";
            Dictionary<string, object> dic = new Dictionary<string, object>() {
                { "@idno", idno }
            };
            return DbService.Query<Func_OM_HGM_ViewGroup>(isMssql, sql, dic).ToList();
        }

        public static IEnumerable<View_OM_HGM_Group_Count> getGroupCount()
        {
            string sql = @"SELECT * FROM [JWETG].[dbo].[View_OM_HGM_Group_Count]";
            return DbService.Query<View_OM_HGM_Group_Count>(isMssql, sql).ToList();
        }
    }
}