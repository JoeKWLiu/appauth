﻿using AppAuth.Models;
using AppAuth.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppAuth.Repositories
{
    public class SubOnedayGroupPermissionRepository
    {
        private static bool isMssql = false;
        public static List<sub_oneday_group_permission> getSubOnedayGroupPermissions()
        {
            string sql = @"select af.appID, af.functionGroupID, af.functionID appFunctionID , sogp.* from sub_oneday_group_permission sogp
                            left join app_function af on sogp.mainUUID = af.uuid";
            return DbService.Query<sub_oneday_group_permission>(isMssql, sql).ToList();
        }

        public static sub_oneday_group_permission insertSubOnedayGroupPermission(sub_oneday_group_permission sogp)
        {
            string sql = @"insert into sub_oneday_group_permission(groupID, mainUUID, functionID, level, isEnabled, comment, uuid)
                           values(@groupID, @mainUUID, @functionID, @level, @isEnabled, @comment, @uuid)
            ";

            DbService.ExecuteNonQuery(sql, sogp);

            sogp.uuid = getSubOnedayGroupPermissions().Where(
                w => w.groupID.Equals(sogp.groupID) &&
                w.mainUUID.Equals(sogp.mainUUID) && w.functionID.Equals(sogp.functionID)
                ).FirstOrDefault().uuid;

            return sogp;
        }

        public static void updateSubOnedayGroupPermissions(IEnumerable<sub_oneday_group_permission> upl)
        {
            string sql = @"update sub_oneday_group_permission set
                            groupID = @groupID,
                            mainUUID = @mainUUID,
                            functionID = @functionID,
                            level = @level,
                            isEnabled = @isEnabled,
                            comment = @comment
                            where uuid = @uuid
            ";

            DbService.ExecuteNonQuery(sql, upl);
        }

        public static void deleteSubOnedayGroupPermission(Guid uuid)
        {
            string sql = @"delete from sub_oneday_group_permission where uuid = @uuid";
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("@uuid", uuid);
            DbService.ExecuteNonQuery(sql, dic);
        }
    }
}