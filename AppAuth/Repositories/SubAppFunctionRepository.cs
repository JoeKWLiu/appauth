﻿using AppAuth.Models;
using AppAuth.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppAuth.Repositories
{
    public class SubAppFunctionRepository
    {
        private static bool isMssql = false;
        public static List<sub_app_function> getSubAppFunctions()
        {
            string sql = @"select saf.*
                            , af.appID, af.functionGroupID, af.functionID appFunctionID,
                            (select count(*) from sub_user_permission where mainUUID = saf.mainUUID and functionID = saf.functionID) ucount,
                            (select count(*) from sub_department_permission where mainUUID = saf.mainUUID and functionID = saf.functionID) dcount,
                            (select count(*) from sub_oneday_group_permission where mainUUID = saf.mainUUID and functionID = saf.functionID) gcount 
                        from sub_app_function saf
                        left join app_function af on saf.mainUUID = af.uuid
                        order by saf.sortOrder, saf.functionID";
            return DbService.Query<sub_app_function>(isMssql, sql).ToList();
        }

        public static sub_app_function insertSubAppFunction(sub_app_function saf)
        {
            string sql = @"insert into sub_app_function(mainUUID, functionID, uuid, functionCode, name, comment, defaultPermission, isEnabled, sortOrder, iconURL, webURL)
                           values(@mainUUID, @functionID, @uuid, @functionCode, @name, @comment, @defaultPermission, @isEnabled, @sortOrder, @iconURL, @webURL)
            ";

            DbService.ExecuteNonQuery(sql, saf);

            saf.uuid = getSubAppFunctions().Where(
                w => w.mainUUID.Equals(saf.mainUUID) && w.functionID.Equals(saf.functionID)
                ).FirstOrDefault().uuid;

            return saf;
        }

        public static void updateSubAppFunctions(IEnumerable<sub_app_function> saf)
        {
            string sql = @"update sub_app_function set
                            mainUUID = @mainUUID,
                            functionID = @functionID,
                            functionCode = @functionCode,
                            name  = @name,
                            comment = @comment,
                            defaultPermission = @defaultPermission,
                            isEnabled = @isEnabled,
                            sortOrder = @sortOrder,
                            iconURL = @iconURL,
                            webURL = @webURL
                            where uuid = @uuid
            ";
            DbService.ExecuteNonQuery(sql, saf);
        }

        public static void deleteSubAppFunction(Guid uuid)
        {
            string sql = @"delete from sub_app_function where uuid = @uuid";
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("@uuid", uuid);
            DbService.ExecuteNonQuery(sql, dic);
        }

        public static string getIconPath()
        {
            var isTest = DbService.isTest();
            var apppem = isTest ? "apppemtest" : "apppem";
            return $"https://appcloud.fpcetg.com.tw/{apppem}/icons/sub_app_function/";
        }
    }
}