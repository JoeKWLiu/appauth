﻿using AppAuth.Models;
using AppAuth.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppAuth.Repositories
{
    public class AdminRepository
    {
        private static bool isMssql = false;
        public static List<admin> getUserPermissions()
        {
            string sql = @"select * from admin";
            return DbService.Query<admin>(isMssql, sql).ToList();
        }

        public static List<admin> getUserPermission(string account)
        {
            string sql = @"select * from admin where account = @account";
            Dictionary<string, object> dic = new Dictionary<string, object>() {
                { "@account", account }
            };
            return DbService.Query<admin>(isMssql, sql, dic).ToList();
        }
    }
}