﻿using AppAuth.Models;
using AppAuth.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppAuth.Repositories
{
    public class AppFunctionRepository
    {
        private static bool isMssql = false;
        public static List<app_function> getAppFunctions()
        {
            string sql = @"select *,
                             (select count(*) from user_permission where appID = app_function.appID and functionGroupID = app_function.functionGroupID and functionID = app_function.functionID) ucount,
                             (select count(*) from department_permission where appID = app_function.appID and functionGroupID = app_function.functionGroupID and functionID = app_function.functionID) dcount,
                             (select count(*) from oneday_group_permission where appID = app_function.appID and functionGroupID = app_function.functionGroupID and functionID = app_function.functionID) gcount,
                             (select count(*) from sub_app_function where mainUUID = app_function.uuid) scount  
                            from app_function
                            /*order by appID, functionGroupID, CASE sortOrder WHEN 0 THEN 1 ELSE 0 END ASC, sortOrder ASC*/
                           order by sortOrder, functionID";
            return DbService.Query<app_function>(isMssql, sql).ToList();
        }

        public static app_function insertAppFunction(app_function dp)
        {
            string sql = @"insert into app_function(appID, functionGroupID, platformID, functionID, functionCode, name, comment, defaultPermission, hasSubFunction, isEnabled, sortOrder, iconURL, webURL, uuid)
                           values(@appID, @functionGroupID, @platformID, @functionID, @functionCode, @name, @comment, @defaultPermission, @hasSubFunction, @isEnabled, @sortOrder, @iconURL, @webURL, @uuid)
            ";

            DbService.ExecuteNonQuery(sql, dp);

            dp.uuid = getAppFunctions().Where(
                w => w.appID.Equals(dp.appID) && w.functionGroupID.Equals(dp.functionGroupID) && w.functionID.Equals(dp.functionID)
                ).FirstOrDefault().uuid;

            return dp;
        }

        public static void updateAppFunctions(IEnumerable<app_function> upl)
        {
            string sql = @"update app_function set
                            appID = @appID,
                            functionGroupID = @functionGroupID,
                            functionID = @functionID,
                            functionCode = @functionCode,
                            name  = @name,
                            comment = @comment,
                            defaultPermission = @defaultPermission,
                            hasSubFunction = @hasSubFunction,
                            isEnabled = @isEnabled,
                            sortOrder = @sortOrder,
                            iconURL = @iconURL,
                            webURL = @webURL
                            where uuid = @uuid
            ";
            DbService.ExecuteNonQuery(sql, upl);
        }

        public static void deleteAppFunction(Guid uuid)
        {
            string sql = @"delete from app_function where uuid = @uuid";
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("@uuid", uuid);
            DbService.ExecuteNonQuery(sql, dic);
        }

        public static string getIconPath()
        {
            var isTest = DbService.isTest();
            var apppem = isTest ? "apppemtest" : "apppem";
            return $"https://appcloud.fpcetg.com.tw/{apppem}/icons/app_function/";
        }
    }
}