﻿using AppAuth.Models;
using AppAuth.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppAuth.Repositories
{
    public class SubDepartmentPermissionRepository
    {
        private static bool isMssql = false;
        public static List<sub_department_permission> getSubDepartmentPermissions()
        {
            string sql = @"select af.appID, af.functionGroupID, af.functionID appFunctionID , coalesce(dpn.fullUnitID, concat(sdp.companyID, sdp.departmentID)) fullUnitID, dpn.departName name, sdp.* from sub_department_permission sdp
                            left join (select Distinct companyID, departID, fullUnitID, departName from account
                            where Length(departName) > 0
                            order by companyID, departID) dpn on sdp.companyID = dpn.companyID and sdp.departmentID = dpn.departID
                            left join app_function af on sdp.mainUUID = af.uuid";
            return DbService.Query<sub_department_permission>(isMssql, sql).ToList();
        }

        public static sub_department_permission insertSubDepartmentPermission(sub_department_permission sdp)
        {
            string sql = @"insert into sub_department_permission(companyID, departmentID, regionID, mainUUID, functionID, level, isEnabled, comment, uuid)
                           values(@companyID, @departmentID, @regionID, @mainUUID, @functionID, @level, @isEnabled, @comment, @uuid)
            ";

            DbService.ExecuteNonQuery(sql, sdp);

            sdp.uuid = getSubDepartmentPermissions().Where(
                w => w.companyID.Equals(sdp.companyID) && w.departmentID.Equals(sdp.departmentID) &&
                w.mainUUID.Equals(sdp.mainUUID) && w.functionID.Equals(sdp.functionID)
                ).FirstOrDefault().uuid;

            return sdp;
        }

        public static void updateSubDepartmentPermissions(IEnumerable<sub_department_permission> upl)
        {
            string sql = @"update sub_department_permission set
                            companyID = @companyID,
                            departmentID = @departmentID,
                            regionID = @regionID,
                            mainUUID = @mainUUID,
                            functionID = @functionID,
                            level = @level,
                            isEnabled = @isEnabled,
                            comment = @comment
                            where uuid = @uuid
            ";

            DbService.ExecuteNonQuery(sql, upl);
        }

        public static void deleteSubDepartmentPermission(Guid uuid)
        {
            string sql = @"delete from sub_department_permission where uuid = @uuid";
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("@uuid", uuid);
            DbService.ExecuteNonQuery(sql, dic);
        }
    }
}