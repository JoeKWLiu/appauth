﻿using AppAuth.Models;
using AppAuth.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppAuth.Repositories
{
    public class AccountRepository
    {
        private static bool isMssql = false;
        public static List<account> getAccounts(string keywords = "", int count = 0)
        {
            string sql = @"select * from account";

            if(!String.IsNullOrEmpty(keywords))
            {
                sql += @" where account like @keywords
                             or name like @keywords ";
            }

            if(count != 0)
            {
                sql += $" limit {count}";
            }

            Dictionary<string, object> dic = new Dictionary<string, object>()
            {
                { "@keywords", $"%{keywords}%"}
            };
            return DbService.Query<account>(isMssql, sql, dic).ToList();
        }

        public static List<account> getAutoAccounts(string keywords = "", int count = 0)
        {
            string sql = @"select account, name, companyID, departName from account";

            if (!String.IsNullOrEmpty(keywords))
            {
                sql += @" where account like @keywords
                             or name like @keywords ";
            }

            if (count != 0)
            {
                sql += $" limit {count}";
            }

            Dictionary<string, object> dic = new Dictionary<string, object>()
            {
                { "@keywords", $"%{keywords}%"}
            };
            return DbService.Query<account>(isMssql, sql, dic).ToList();
        }

        public static List<account> getFullUnits(string keywords = "", int count = 0)
        {
            string sql = @"select distinct fullUnitID, departName from account
                                where fullUnitId != '' and departName != '' ";

            if (!String.IsNullOrEmpty(keywords))
            {
                sql += @" and ( fullUnitID like @keywords or departName like @keywords)";
            }

            sql += @" order by fullUnitID ";

            if (count != 0)
            {
                sql += $" limit {count}";
            }

            Dictionary<string, object> dic = new Dictionary<string, object>()
            {
                { "@keywords", $"%{keywords}%"}
            };

            return DbService.Query<account>(isMssql, sql, dic).ToList();
        }
    }
}