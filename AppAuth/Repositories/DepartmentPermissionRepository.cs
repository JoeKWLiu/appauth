﻿using AppAuth.Models;
using AppAuth.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppAuth.Repositories
{
    public class DepartmentPermissionRepository
    {
        private static bool isMssql = false;
        public static List<department_permission> getDepartmentPermissions()
        {
            string sql = @"select coalesce(dpn.fullUnitID, concat(dp.companyID, dp.departmentID)) fullUnitID, dpn.departName name, dp.* from department_permission dp
                            left join (select Distinct companyID, departID, fullUnitID, departName from account
                            where Length(departName) > 0
                            order by companyID, departID) dpn on dp.companyID = dpn.companyID and dp.departmentID = dpn.departID";
            return DbService.Query<department_permission>(isMssql, sql).ToList();
        }

        public static department_permission insertDepartmentPermission(department_permission dp)
        {
            string sql = @"insert into department_permission(companyID, departmentID, regionID, appID, functionGroupID, platformID, functionID, level, isEnabled, comment, uuid)
                           values(@companyID, @departmentID, @regionID, @appID, @functionGroupID, @platformID, @functionID, @level, @isEnabled, @comment, @uuid)
            ";

            DbService.ExecuteNonQuery(sql, dp);

            dp.uuid = getDepartmentPermissions().Where(
                w => w.companyID.Equals(dp.companyID) && w.departmentID.Equals(dp.departmentID) &&
                w.appID.Equals(dp.appID) && w.functionGroupID.Equals(dp.functionGroupID) && w.functionID.Equals(dp.functionID)
                ).FirstOrDefault().uuid;

            return dp;
        }

        public static void updateDepartmentPermissions(IEnumerable<department_permission> upl)
        {
            string sql = @"update department_permission set
                            companyID = @companyID,
                            departmentID = @departmentID,
                            regionID = @regionID,
                            appID = @appID,
                            functionGroupID  = @functionGroupID,
                            platformID = @platformID,
                            functionID = @functionID,
                            level = @level,
                            isEnabled = @isEnabled,
                            comment = @comment
                            where uuid = @uuid
            ";

            DbService.ExecuteNonQuery(sql, upl);
        }

        public static void deleteDepartmentPermission(Guid uuid)
        {
            string sql = @"delete from department_permission where uuid = @uuid";
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("@uuid", uuid);
            DbService.ExecuteNonQuery(sql, dic);
        }
    }
}