﻿using AppAuth.Models;
using AppAuth.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppAuth.Repositories
{
    public class LogRepository
    {
        public static void Log(log lg)
        {
            string sql = @"insert log(CreatedBy, Controller, Action, Param, Status, Msg, CreatedOn)
                            values(@CreatedBy, @Controller, @Action, @Param, @Status, @Msg, @CreatedOn)
            ";

            DbService.ExecuteNonQuery(sql, lg);
        }
    }
}