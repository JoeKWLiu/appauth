﻿using AppAuth.Models;
using AppAuth.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppAuth.Repositories
{
    public class AppBaseRepository
    {
        private static bool isMssql = false;
        public static IEnumerable<app_base> getAppBases(IEnumerable<int> appIDs)
        {
            string sql = @"select * from app_base where appID IN @appIDs
                            order by appID";
            Dictionary<string, object> dic = new Dictionary<string, object>() {
                { "@appIDs", appIDs }
            };

            if (appIDs.Contains(0))
            {
                sql = @"select * from app_base order by appID";
            }

            return DbService.Query<app_base>(isMssql, sql, dic).ToList();
        }
    }
}