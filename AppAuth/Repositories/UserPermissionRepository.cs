﻿using AppAuth.Models;
using AppAuth.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppAuth.Repositories
{
    public class UserPermissionRepository
    {
        private static bool isMssql = false;
        public static List<user_permission> getUserPermissions()
        {
            string sql = @"select up.*, a.name, a.companyID, a.departName
                            from user_permission up
                        left join account a on up.user = a.account";
            return DbService.Query<user_permission>(isMssql, sql).ToList();
        }

        public static user_permission insertUserPermission(user_permission up)
        {
            string sql = @"insert into user_permission(user, appID, functionGroupID, platformID, functionID, level, isEnabled, comment)
                           values(@user, @appID, @functionGroupID, @platformID, @functionID, @level, @isEnabled, @comment)
            ";

            DbService.ExecuteNonQuery(sql, up);

            //up.uuid = getUserPermissions().Where(
            //    w => w.user.Equals(up.user) && w.appID.Equals(up.appID) &&
            //    w.functionGroupID.Equals(up.functionGroupID) && w.functionID.Equals(up.functionID)
            //).FirstOrDefault().uuid;

            //return up;

            return getUserPermissions().Where(
                w => w.user.Equals(up.user) && w.appID.Equals(up.appID) &&
                w.functionGroupID.Equals(up.functionGroupID) && w.functionID.Equals(up.functionID)
            ).FirstOrDefault();
        }

        public static object updateUserPermissions(IEnumerable<user_permission> upl)
        {
            string sql = @"update user_permission set
                            user = @user,
                            appID = @appID,
                            functionGroupID  = @functionGroupID,
                            platformID = @platformID,
                            functionID = @functionID,
                            level = @level,
                            isEnabled = @isEnabled,
                            comment = @comment
                            where uuid = @uuid
            ";

            DbService.ExecuteNonQuery(sql, upl);

            return getUserPermissions().Where(w => upl.Select(s => s.uuid).Contains(w.uuid));
        }

        public static void deleteUserPermission(Guid uuid)
        {
            string sql = @"delete from user_permission where uuid = @uuid";
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("@uuid", uuid);
            DbService.ExecuteNonQuery(sql, dic);
        }

        //TODO ??!
        public static void deleteAllUserPermission(RequestModel rm)
        {
            if (rm.subFunctionID.Equals(0))
            {
                string sql = @"delete from user_permission where appID = @appID and functionGroupID = @functionGroupID and functionID = @functionID";
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic.Add("@appID", rm.appID);
                dic.Add("@functionGroupID", rm.functionGroupID);
                dic.Add("@functionID", rm.functionID);
                DbService.ExecuteNonQuery(sql, dic);
            }else
            {
                string sql = @"delete from user_permission where appID = @appID and functionGroupID = @functionGroupID and functionID = @functionID";
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic.Add("@appID", rm.appID);
                dic.Add("@functionGroupID", rm.functionGroupID);
                dic.Add("@functionID", rm.functionID);
                DbService.ExecuteNonQuery(sql, dic);
            }
            
        }
    }
}