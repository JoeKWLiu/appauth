﻿using AppAuth.Models;
using AppAuth.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppAuth.Repositories
{
    public class OnedayGroupPermissionRepository
    {
        private static bool isMssql = false;
        public static List<oneday_group_permission> getOnedayGroupPermissions()
        {
            string sql = @"select * from oneday_group_permission";
            return DbService.Query<oneday_group_permission>(isMssql, sql).ToList();
        }

        public static oneday_group_permission insertOnedayGroupPermission(oneday_group_permission ogp)
        {
            string sql = @"insert into oneday_group_permission(groupID, appID, functionGroupID, platformID, functionID, level, isEnabled, comment, uuid)
                           values(@groupID, @appID, @functionGroupID, @platformID, @functionID, @level, @isEnabled, @comment, @uuid)
            ";
            DbService.ExecuteNonQuery(sql, ogp);

            ogp.uuid = getOnedayGroupPermissions().Where(
                w => w.groupID.Equals(ogp.groupID) &&
                w.appID.Equals(ogp.appID) && w.functionGroupID.Equals(ogp.functionGroupID) && w.functionID.Equals(ogp.functionID)
                ).FirstOrDefault().uuid;

            return ogp;
        }

        public static void updateOnedayGroupPermissions(IEnumerable<oneday_group_permission> upl)
        {
            string sql = @"update oneday_group_permission set
                            groupID = @groupID,
                            appID = @appID,
                            functionGroupID  = @functionGroupID,
                            platformID = @platformID,
                            functionID = @functionID,
                            level = @level,
                            isEnabled = @isEnabled,
                            comment = @comment
                            where uuid = @uuid
            ";

            DbService.ExecuteNonQuery(sql, upl);
        }

        public static void deleteOnedayGroupPermission(Guid uuid)
        {
            string sql = @"delete from oneday_group_permission where uuid = @uuid";
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("@uuid", uuid);
            DbService.ExecuteNonQuery(sql, dic);
        }
    }
}