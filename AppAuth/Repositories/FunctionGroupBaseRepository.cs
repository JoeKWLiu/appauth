﻿using AppAuth.Models;
using AppAuth.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppAuth.Repositories
{
    public class FunctionGroupBaseRepository
    {
        private static bool isMssql = false;
        public static List<function_group_base> getFunctionGroupBases()
        {
            List<function_group_base> result = new List<function_group_base>();

            string sql = @"select *,
                             (select count(*) from user_permission where appID = function_group_base.appID and functionGroupID = function_group_base.functionGroupID and functionID = 0) ucount,
                             (select count(*) from department_permission where appID = function_group_base.appID and functionGroupID = function_group_base.functionGroupID and functionID = 0) dcount,
                             (select count(*) from oneday_group_permission where appID = function_group_base.appID and functionGroupID = function_group_base.functionGroupID and functionID = 0) gcount,
                             (select count(*) from app_function where appID = function_group_base.appID and functionGroupID = function_group_base.functionGroupID) scount 
	                            from function_group_base
                            order by sortOrder, functionGroupID";
            result = DbService.Query<function_group_base>(isMssql, sql).ToList();
            
            return result;
        }

        public static void updateFunctionGroupBases(IEnumerable<function_group_base> fgList)
        {
            string sql = @"update function_group_base set
                            appID = @appID,
                            functionGroupID  = @functionGroupID,
                            functionGroupCode = @functionGroupCode,
                            name = @name,
                            sortOrder = @sortOrder,
                            isEnabled = @isEnabled
                            where uuid = @uuid
            ";

            DbService.ExecuteNonQuery(sql, fgList);

        }

        public static void insertFunctionGroupBase(function_group_base fg)
        {
            string sql = @"insert function_group_base(appID, functionGroupID, functionGroupCode, name, sortOrder, uuid, isEnabled)
                            values(@appID, @functionGroupID, @functionGroupCode, @name, @sortOrder, @uuid, @isEnabled)
            ";

            DbService.ExecuteNonQuery(sql, fg);

        }

        public static void deleteFunctionGroupBase(Guid uuid)
        {
            string sql = @"delete from function_group_base
                            where uuid = @uuid
            ";

            Dictionary<string, object> dic = new Dictionary<string, object>()
            {
                { "@uuid", uuid}
            };

            DbService.ExecuteNonQuery(sql, dic);

        }

    }
}