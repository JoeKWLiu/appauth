﻿using Dapper;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppPrivilege.Services
{
    public class DbService
    {
        private static Dictionary<string, string> dbInfo = new Dictionary<string, string>()
        {
            { "joe", "10.153.196.100" },
        };

        private static string dbAccount = "joe";
        private static string dbPassword = "`1qaz2wsx";
        private static string strConn = String.Empty;

        public static List<T> Query<T>(string dbName, string sql, Dictionary<string, object> dic = null)
        {
            List<T> result = new List<T>();
            DbInit(dbName);

            using (MySqlConnection conn = new MySqlConnection(strConn))
            {
                result = conn.Query<T>(sql, dic).ToList();
            }
            return result;
        }

        public static void ExecuteNonQuery(string dbName, string sql, Dictionary<string, object> dic = null)
        {
            DbInit(dbName);
            using (MySqlConnection conn = new MySqlConnection(strConn))
            {
                conn.Execute(sql, dic);
            }
        }

        public static T ExecuteScalar<T>(string dbName, string sql, Dictionary<string, object> dic = null)
        {
            DbInit(dbName);
            using (MySqlConnection conn = new MySqlConnection(strConn))
            {
                return (T)conn.ExecuteScalar(sql, dic);
            }
        }

        private static void DbInit(string dbName)
        {
            strConn = String.Format("data source ={0}; initial catalog = {1}; user id = {2}; password = {3}; Connection Timeout=180"
                             , dbInfo[dbName]
                             , dbName
                             , dbAccount
                             , dbPassword);
        }
    }
}