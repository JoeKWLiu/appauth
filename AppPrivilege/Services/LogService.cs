﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;

namespace AppPrivilege.Services
{
    public class LogService
    {
        private static string loginUrl = "https://appcloud.fpcetg.com.tw/mobilefpc/api/common/login";

        public static LoginResponse Login(LoginRequest request)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string jsonText = serializer.Serialize(request);
            using (WebClient wc = new WebClient())
            {
                try
                {
                    wc.Encoding = Encoding.UTF8;
                    wc.Headers.Add(HttpRequestHeader.ContentType, "application/json");
                    var response = wc.UploadData(loginUrl, "POST", Encoding.UTF8.GetBytes(jsonText));
                    var login = (LoginResponse)serializer.Deserialize(Encoding.UTF8.GetString(response), typeof(LoginResponse));

                    if (login.result == "SUCCESS")
                    {
                    }

                    return login;
                }
                catch (WebException ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }

    }

    public class LoginRequest
    {
        public string account { get; set; }
        public string password { get; set; }
        public int platformNumber { get; set; } = 3;
        public string appVersion { get; set; } = "9.9.9";
        public string model { get; set; } = "Web";
        public string OSVersion { get; set; } = "1.0.0";
        public string deviceID { get; set; } = "123467890";
        public int isDebugMode { get; set; } = 0;
    }

    public class LoginResponse
    {
        public string result { get; set; }
        public int aid { get; set; }
        public string account { get; set; }
        public string name { get; set; }
        public string accessToken { get; set; }
        public string refreshToken { get; set; }
    }

}