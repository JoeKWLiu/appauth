﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppPrivilege.Models
{
    public class Admin
    {
        public string account { get; set; }
        public int appID { get; set; }
        //public int functionGroupID { get; set; }
        public int platformID { get; set; }
        public int functionID { get; set; }
        public bool isAdmin { get; set; }
        public bool functionGroupIDBegin { get; set; }
        public bool functionGroupIDEnd { get; set; }
    }

}