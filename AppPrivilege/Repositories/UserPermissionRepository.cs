﻿using AppPrivilege.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AppPrivilege.Services;

namespace AppPrivilege.Repositories
{
    public class UserPermissionRepository
    {
        public static List<UserPermission> getUserPermission()
        {
            string sql = @"select * from user_permission";
            return DbService.Query<UserPermission>("joe", sql).ToList();
        }

        public static UserPermission createUserPermission(UserPermission up)
        {
            string sql = @"insert into user_permission(user, appID, functionGroupID, platformID, functionID, level, isEnabled, comment, uuid)
                           values(@user, @appID, @functionGroupID, @platformID, @functionID, @level, @isEnabled, @comment, @uuid)
            ";

            up.uuid = new Guid().ToString();

            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("@user", up.user);
            dic.Add("@appID", up.appID);
            dic.Add("@functionGroupID", up.functionGroupID);
            dic.Add("@platformID", up.platformID);
            dic.Add("@functionID", up.functionID);
            dic.Add("@level", up.level);
            dic.Add("@isEnabled", up.isEnabled);
            dic.Add("@comment", up.comment);
            dic.Add("@uuid", up.uuid);
            DbService.ExecuteNonQuery("joe", sql, dic);
            return up;
        }

        public static void updateUserPermission(UserPermission up)
        {
            string sql = @"update user_permission set
                            user = @user,
                            appID = @appID,
                            functionGroupID  = @functionGroupID,
                            platformID = @platformID,
                            functionID = @functionID,
                            level = @level,
                            isEnabled = @isEnabled,
                            comment = @comment
                            where uuid = @uuid   
            ";
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("@user", up.user);
            dic.Add("@appID", up.appID);
            dic.Add("@functionGroupID", up.functionGroupID);
            dic.Add("@platformID", up.platformID);
            dic.Add("@functionID", up.functionID);
            dic.Add("@level", up.level);
            dic.Add("@isEnabled", up.isEnabled);
            dic.Add("@comment", up.comment);
            dic.Add("@uuid", up.uuid);

            DbService.ExecuteNonQuery("joe", sql, dic);
        }

        public static void deleteUserPermission(string uuid)
        {
            string sql = @"delete from user_permission where uuid = @uuid";
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("@uuid", uuid);
            DbService.ExecuteNonQuery("joe", sql, dic);
        }
    }
}