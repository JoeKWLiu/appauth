﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Query;
using System.Web.Http.OData.Routing;
using AppPrivilege.Models;
using AppPrivilege.Repositories;
using Microsoft.Data.OData;

namespace AppPrivilege.Controllers
{

    public class UserPermissionController : ODataController
    {
        private static ODataValidationSettings _validationSettings = new ODataValidationSettings();


        [EnableQuery]
        // GET: odata/UserPermission
        public IHttpActionResult GetUserPermission(ODataQueryOptions<UserPermission> queryOptions, string accessToken)
        {
            // validate the query.
            try
            {
                queryOptions.Validate(_validationSettings);
            }
            catch (ODataException ex)
            {
                return BadRequest(ex.Message);
            }

            if ("1234".Equals(accessToken))
            {
                return Ok<IEnumerable<UserPermission>>(UserPermissionRepository.getUserPermission());
                //return StatusCode(HttpStatusCode.NotImplemented);
            }
            else
            {
                return BadRequest();
            }
        }

        // PUT: odata/UserPermission(5)
        public IHttpActionResult Put([FromODataUri] string key, [FromBody] UserPermission up)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            // TODO: Get the entity here.

            // delta.Put(userPermissionRepository);

            // TODO: Save the patched entity.
            UserPermissionRepository.updateUserPermission(up);
            return Updated(up);
            //return StatusCode(HttpStatusCode.NotImplemented);
        }

        // POST: odata/UserPermission
        public IHttpActionResult Post(UserPermission userPermissionRepository)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            // TODO: Add create logic here.

            return Created(UserPermissionRepository.createUserPermission(userPermissionRepository));
            //return StatusCode(HttpStatusCode.NotImplemented);
        }

        // PATCH: odata/UserPermission(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] string key, Delta<UserPermission> delta)
        {
            Validate(delta.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            // TODO: Get the entity here.

            // delta.Patch(userPermissionRepository);

            // TODO: Save the patched entity.

            // return Updated(userPermissionRepository);
            return StatusCode(HttpStatusCode.NotImplemented);
        }

        // DELETE: odata/UserPermission(5)
        public IHttpActionResult Delete([FromODataUri] string key)
        {
            // TODO: Add delete logic here.

            UserPermissionRepository.deleteUserPermission(key);
            return StatusCode(HttpStatusCode.NoContent);
            //return StatusCode(HttpStatusCode.NotImplemented);
        }
    }
}
